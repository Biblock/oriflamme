from collections.abc import Iterator
from typing import Dict, Set

from src.engine.card import Card
from src.engine.enums import Position
from src.engine.event_manager import CardDiscardedEvent, CardMovedEvent
from src.engine.exceptions import CanNotMoveOnOtherCardError, PlacementOnTargetCardIsNoneError, \
    CanNotPlaceOnOtherPlayerCardError, NoSuchCardOnBoardError, CardAlreadyOnBoardError, CannotTargetCoveredCardError


class Slot:
    def __init__(self, previous_slot=None, next_slot=None):
        self.previous_slot = previous_slot
        self.next_slot = next_slot


class CardSlot(Slot):
    def __init__(self, card, previous_slot=None, next_slot=None, under_slot=None):
        super().__init__(previous_slot, next_slot)
        self.card = card
        self.under_slot = under_slot
        self.covered = False

        if previous_slot is not None:
            previous_slot.next_slot = self
        if next_slot is not None:
            next_slot.previous_slot = self
        if under_slot is not None:
            under_slot.covered = True
            # Should never be useful with the current cards because no card can become a covered card and still
            # be asked who's next
            # But who knows, an extension... a card that would allow to place a card on top of it and activate it
            # immediately for example
            under_slot.next_slot = self

    def remove(self):
        if self.under_slot is None:
            self.previous_slot.next_slot = self.next_slot
            self.next_slot.previous_slot = self.previous_slot
        else:
            self.previous_slot.next_slot = self.under_slot
            self.next_slot.previous_slot = self.under_slot
            self.under_slot.next_slot = self.next_slot
            self.under_slot.previous_slot = self.previous_slot
            self.under_slot.covered = False

            self.next_slot = self.under_slot


class Board:
    _cards_slots: Dict[Card, CardSlot]

    def __init__(self, events=None):
        if events is None:
            events = []
        self._cards_slots = {}
        self.first_card_slot = Slot()
        self.last_card_slot = Slot(self.first_card_slot)
        self.first_card_slot.next_slot = self.last_card_slot
        self.events = events

    def __iter__(self):
        return BoardIterator(self.first_card_slot)

    def place(self, card: Card, position: Position, target_card: Card = None):
        if position == Position.ON and target_card is None:
            raise PlacementOnTargetCardIsNoneError

        if card in self._cards_slots.keys():
            raise CardAlreadyOnBoardError

        if position == Position.ON:
            target_slot = self.targeted_card_slot(target_card)
            if target_card.player != card.player:
                raise CanNotPlaceOnOtherPlayerCardError
            new_card_slot = CardSlot(card, previous_slot=target_slot.previous_slot, next_slot=target_slot.next_slot,
                                     under_slot=target_slot)
        else:
            target_slot = self.targeted_card_slot(target_card) if target_card is not None \
                else (
                self.last_card_slot.previous_slot if position == Position.RIGHT else self.first_card_slot.next_slot)

            previous_card = target_slot if position == Position.RIGHT else target_slot.previous_slot
            next_card = target_slot.next_slot if position == Position.RIGHT else target_slot
            new_card_slot = CardSlot(card, previous_slot=previous_card, next_slot=next_card)

        self._cards_slots[card] = new_card_slot

    def remove(self, card: Card):
        card_slot = self.targeted_card_slot(card)
        card_slot.remove()
        del self._cards_slots[card]

    def discard(self, card: Card):
        try:
            self.remove(card)
            self.events.append(CardDiscardedEvent(card))
        except NoSuchCardOnBoardError:
            pass

    def move(self, card, position, destination_card):
        if position == Position.ON:
            raise CanNotMoveOnOtherCardError

        if card == destination_card:
            destination_slot = self.targeted_card_slot(destination_card)
            if destination_slot.under_slot is None:
                return
            destination_card = destination_slot.under_slot.card
        self.remove(card)
        self.place(card, position, destination_card)
        self.events.append(CardMovedEvent(card, destination_card, position))

    def targeted_card_slot(self, card: Card):
        try:
            slot = self._cards_slots[card]
            if slot.covered:
                raise CannotTargetCoveredCardError
            return slot
        except KeyError:
            raise NoSuchCardOnBoardError

    def adjacent_cards(self, card) -> Set[Card]:
        slot = self.targeted_card_slot(card)
        adjacent_cards = set()
        for adj_slot in [slot.previous_slot, slot.next_slot]:
            try:
                card = adj_slot.card
            except AttributeError:
                pass
            else:
                adjacent_cards.add(card)
        return adjacent_cards

    def extremities(self) -> Set[Card]:
        extreme_cards = set()
        for adj_slot in [self.first_card_slot.next_slot, self.last_card_slot.previous_slot]:
            try:
                card = adj_slot.card
            except AttributeError:
                pass
            else:
                extreme_cards.add(card)
        return extreme_cards

    @property
    def stacks(self):
        stacks = []
        stack_slot = self.first_card_slot

        while isinstance(stack_slot := stack_slot.next_slot, CardSlot):
            stack = [stack_slot.card]
            card_slot = stack_slot
            while (card_slot := card_slot.under_slot) is not None:
                stack.insert(0, card_slot.card)
            stacks.append(stack)
        return stacks

    @property
    def cards(self) -> Set[Card]:
        return set(self._cards_slots.keys())

    @property
    def uncovered_cards(self) -> Set[Card]:
        return {stack[-1] for stack in self.stacks}


class BoardIterator(Iterator):
    def __init__(self, first_slot: Slot):
        self.current_slot = first_slot

    def __next__(self) -> Card:
        next_slot = self.current_slot.next_slot

        if not isinstance(next_slot, CardSlot):
            raise StopIteration

        self.current_slot = next_slot
        return self.current_slot.card

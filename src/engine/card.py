from src.engine.ability import Archer, Soldat, Espion, Heritier, Assassinat, DecretRoyal, Seigneur, Changeforme, \
    Embuscade, Complot
from src.engine.enums import CardName
from src.engine.event_manager import RevealedCardEvent, CardInfluenceUpdateEvent, RevealedCardActivationEvent
from src.engine.user_input import UserInput


class Card:
    ABILITIES = {
        CardName.ARCHER: Archer,
        CardName.SOLDAT: Soldat,
        CardName.ESPION: Espion,
        CardName.HERITIER: Heritier,
        CardName.ASSASSINAT: Assassinat,
        CardName.DECRET_ROYAL: DecretRoyal,
        CardName.SEIGNEUR: Seigneur,
        CardName.CHANGEFORME: Changeforme,
        CardName.EMBUSCADE: Embuscade,
        CardName.COMPLOT: Complot,
    }

    def __init__(self, player, card_name: CardName):
        self._influence = 0
        self.player = player
        self.name = card_name
        self.ability = self.ABILITIES[card_name]
        self.revealed = False

    @property
    def influence(self):
        return self._influence

    @influence.setter
    def influence(self, value):
        difference = value - self._influence
        self._influence = value
        if difference != 0:
            self.player.events.append(CardInfluenceUpdateEvent(self, difference, value))

    def activate(self, board, user_input: UserInput = None):
        self.player.events.append(RevealedCardActivationEvent(self))
        self.ability.activate(self, board, user_input)

    def validate(self, board, user_input: UserInput = None):
        self.ability.validate(self, board, user_input)

    def reveal(self):
        self.revealed = True
        self.player.events.append(RevealedCardEvent(self))
        self.ability.reveal(self)

    def __str__(self):
        return f"{self.name} ({self.player})"

from enum import Enum, auto


class Color(Enum):
    RED = auto()
    BLUE = auto()
    GREEN = auto()
    BLACK = auto()
    YELLOW = auto()


class Position(Enum):
    LEFT = auto()
    RIGHT = auto()
    ON = auto()


class CardName(Enum):
    ARCHER = auto()
    SOLDAT = auto()
    ESPION = auto()
    HERITIER = auto()
    ASSASSINAT = auto()
    DECRET_ROYAL = auto()
    SEIGNEUR = auto()
    CHANGEFORME = auto()
    EMBUSCADE = auto()
    COMPLOT = auto()


class RevealChoice(Enum):
    REVEAL = auto()
    DO_NOT_REVEAL = auto()

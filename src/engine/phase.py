from typing import List

from src.engine.board import Board
from src.engine.enums import RevealChoice
from src.engine.event_manager import ResolutionPlayerTurnEvent, PlacementPlayerTurnEvent, \
    PlayerChoseRevealEvent, PlacementPhaseStartingEvent, ResolutionPhaseStartingEvent
from src.engine.exceptions import NotPlayerTurnError, PhaseNotStartedError, InvalidUserInputError, \
    NeedUserInputError, PhaseOverError, MissingPlayerOfPhaseUserInputError
from src.engine.player import Player
from src.engine.user_input import UserInput, PlacementUserInput, RevealUserInput


class Phase:
    def __init__(self, events):
        self.events = events
        self.current_prompt_input_type = None

    def proceed(self, player=None, user_input: UserInput = None):
        if not self.is_started:
            raise PhaseNotStartedError
        if self.is_over:
            raise PhaseOverError

        if user_input is not None:
            if player is None:
                raise MissingPlayerOfPhaseUserInputError
            if player != self.player_turn:
                raise NotPlayerTurnError

        try:
            self._validate(user_input)
            self._proceed(user_input)
            self.current_prompt_input_type = None
        except NeedUserInputError as err:
            self.current_prompt_input_type = err.event.prompt_type
            self.events.append(err.event)
            raise
        else:
            self.proceed()

    def _proceed(self, user_input: UserInput = None):
        raise NotImplementedError

    def start(self):
        self._start()
        self.proceed()

    @property
    def is_over(self):
        raise NotImplementedError

    @property
    def is_started(self):
        raise NotImplementedError

    def _validate(self, user_input):
        raise NotImplementedError

    @property
    def player_turn(self):
        raise NotImplementedError

    def _start(self):
        raise NotImplementedError


class PlacementPhase(Phase):
    def __init__(self, events, players: List[Player], board: Board):
        super().__init__(events)
        self.remaining_players = players
        self.board = board
        self.started = False

    def _proceed(self, user_input: PlacementUserInput = None):
        current_player = self.player_turn
        current_player.play_card_name(self.board, user_input.card_name, user_input.position,
                                      user_input.target_card)
        self.remaining_players.remove(current_player)

    def _start(self):
        self.started = True
        self.events.append(PlacementPhaseStartingEvent())

    @property
    def is_over(self):
        return not self.remaining_players

    @property
    def is_started(self):
        return self.started

    @property
    def player_turn(self):
        try:
            return self.remaining_players[0]
        except IndexError:
            return None

    def _validate(self, user_input: PlacementUserInput):
        if user_input is None:
            raise NeedUserInputError(PlacementPlayerTurnEvent(self.player_turn))

        if not isinstance(user_input, PlacementUserInput):
            raise InvalidUserInputError


class ResolutionPhase(Phase):
    def __init__(self, events, board):
        super().__init__(events)
        self.board = board
        self.board_iterator = iter(board)
        self.active_card = None
        self.over = False

    def _proceed(self, user_input: RevealUserInput = None):
        if not self.active_card.revealed:
            self.events.append(PlayerChoseRevealEvent(self.active_card, user_input.reveal_choice))
            if user_input.reveal_choice == RevealChoice.REVEAL:
                self.active_card.reveal()
            else:
                self.active_card.influence += 1
                self._next_card()

        else:
            self.active_card.activate(self.board, user_input)
            self._next_card()

    def _next_card(self):
        try:
            self.active_card = next(self.board_iterator)
        except StopIteration:
            self.over = True

    def _validate(self, user_input: UserInput):
        if not self.active_card.revealed:
            if user_input is None:
                raise NeedUserInputError(ResolutionPlayerTurnEvent(self.active_card))
            elif not isinstance(user_input, RevealUserInput):
                raise InvalidUserInputError
        else:
            self.active_card.validate(self.board, user_input)

    @property
    def player_turn(self):
        return self.active_card.player

    @property
    def is_over(self):
        return self.over

    def _start(self):
        self.events.append(ResolutionPhaseStartingEvent())
        self._next_card()

    @property
    def is_started(self):
        return self.active_card is not None or self.is_over

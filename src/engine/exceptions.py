from src.engine.event_manager import GameEvent


class OriflammeError(Exception):
    pass


class GameNotStartedError(OriflammeError):
    pass


class CanNotPlaceOnOtherPlayerCardError(OriflammeError):
    pass


class CanNotMoveOnOtherCardError(OriflammeError):
    pass


class NoSuchCardOnBoardError(OriflammeError):
    pass


class CardAlreadyOnBoardError(OriflammeError):
    pass


class PlacementOnTargetCardIsNoneError(OriflammeError):
    pass


class CannotTargetCoveredCardError(OriflammeError):
    pass


class NumberOfCardsAskedTooHigh(OriflammeError):
    pass


class CardNotInPlayerHandError(OriflammeError):
    pass


class InvalidUserInputError(OriflammeError):
    pass


class NotPlayerTurnError(OriflammeError):
    pass


class PhaseNotStartedError(OriflammeError):
    pass


class PhaseOverError(OriflammeError):
    pass


class RoundOverError(OriflammeError):
    pass


class MissingPlayerOfPhaseUserInputError(OriflammeError):
    pass


class GameAlreadyStartedError(OriflammeError):
    pass


class GameOverError(OriflammeError):
    pass


class NeedUserInputError(OriflammeError):
    def __init__(self, event: GameEvent):
        self.event = event


class MurderCounteredError(OriflammeError):
    pass

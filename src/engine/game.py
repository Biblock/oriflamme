from random import sample

from src.engine.board import Board
from src.engine.enums import Color
from src.engine.event_manager import GameOverEvent, RoundStartingEvent, GameStatusEvent
from src.engine.exceptions import GameNotStartedError, GameAlreadyStartedError, RoundOverError, GameOverError
from src.engine.player import Player
from src.engine.round import Round
from src.engine.user_input import UserInput


class Game:
    def __init__(self, players_number, rounds_number=6, players_cards_number=7):
        self.round_counter = 0
        self.events = []
        self.board = Board(self.events)
        # noinspection PyTypeChecker
        self.players = [Player(self.events, color, players_cards_number) for color in
                        sample(list(Color), players_number)]
        self.rounds = []
        round_players = self.players
        for _ in range(rounds_number):
            self.rounds.append(Round(self.events, round_players.copy(), self.board))
            round_players.append(round_players.pop(0))

        self.is_started = False
        self.is_over = False

    @property
    def active_round(self):
        return self.rounds[self.round_counter]

    def start(self):
        if not self.is_started:
            self.is_started = True
            self.events.append(GameStatusEvent())
            self.start_new_round()
        else:
            raise GameAlreadyStartedError

    def start_new_round(self):
        self.events.append(RoundStartingEvent(self.round_counter))
        self.proceed()

    def proceed(self, player=None, user_input: UserInput = None):
        if not self.is_started:
            raise GameNotStartedError
        if self.is_over:
            raise GameOverError
        try:
            self.active_round.proceed(player, user_input)
        except RoundOverError:
            self.round_counter += 1
            if self.round_counter == len(self.rounds):
                self.is_over = True
                self.events.append(GameOverEvent())
            else:
                self.start_new_round()

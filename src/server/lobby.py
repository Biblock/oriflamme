from random import shuffle

from src.engine.game import Game as EngineGame
from src.server.chat import send_lobby_message, MAX_MESSAGE_LENGTH
from src.server.engine_events import game_status
from src.server.events import player_joined_game_event, player_left_game_event, player_joined_lobby_event, \
    player_session_token_event, game_created_event, no_such_logged_player_event, disconnected_event, \
    game_already_exists_event, max_player_limit_event, player_already_in_a_game_event, TerminalEventError, \
    game_deleted_event, player_is_not_game_leader_event, \
    not_enough_players_event, game_already_started_event, NoSuchGame, chat_message_too_long_event, lobby_chat_message_event
from src.server.game import Game
from src.server.handler import ActionHandler
from src.server.player import Player


class Lobby(ActionHandler):
    def __init__(self, server):
        self.server = server
        self.players = []
        self._games = []
        self.number_player_cards = 7

    @property
    def games(self):
        return tuple(game for game in self._games if not game.over)

    @property
    def logged_player(self):
        return [player for player in self.players if player.logged]

    @property
    def disconnected_player(self):
        return [player for player in self.players if not player.logged]

    @ActionHandler.action(logged=False)
    def login(self, ws_wrapper, name, session_token=None):
        logging_player = None
        if session_token:
            try:
                logging_player = [player for player in self.players if player.session_token == session_token][0]
            except IndexError:
                session_token = None
            else:
                if logging_player.logged:
                    self.logoff(logging_player, send_disconnected_player=True)
                logging_player.name = name
        if not logging_player:
            logging_player = Player(name)

        logging_player.login(ws_wrapper, session_token)
        if logging_player not in self.players:
            self.players.append(logging_player)
        logging_player.send(player_session_token_event(logging_player, self))
        self.send_logged_player(player_joined_lobby_event(logging_player))
        for game in self.games:
            if game.started and not game.over and logging_player in game.players:
                game_status(game, logging_player)

    @ActionHandler.action()
    def create_game(self, player, game_name):
        self.check_player_in_game(player)
        try:
            self.find_game(game_name)
        except NoSuchGame:
            game = Game(game_name, player)
            self._games.append(game)
            self.send_logged_player(game_created_event(game))
            self.send_logged_player(player_joined_game_event(player, game))
        else:
            raise TerminalEventError(game_already_exists_event(game_name))

    @ActionHandler.action()
    def join_game(self, player, game_name):
        self.check_player_in_game(player)
        game = self.find_game(game_name)
        if len(game.players) == Game.MAXIMUM_PLAYER_NUMBER:
            raise TerminalEventError(max_player_limit_event(Game.MAXIMUM_PLAYER_NUMBER))
        if game.started:
            raise TerminalEventError(game_already_started_event(game))
        game.add_player(player)
        self.send_logged_player(player_joined_game_event(player, game))

    @ActionHandler.action()
    def leave_game(self, player, game_name):
        game = self.find_game(game_name)
        game.remove_player(player)
        if not game.players or game.started:
            self.send_logged_player(game_deleted_event(game.name))
            self._games.remove(game)
        else:
            self.send_logged_player(player_left_game_event(player, game))

    @ActionHandler.action()
    def start_game(self, player, game_name):
        game = self.find_game(game_name)
        if game.started:
            raise TerminalEventError(game_already_started_event(game))
        if game.leader != player:
            raise TerminalEventError(player_is_not_game_leader_event(player))
        if len(game.players) < Game.MINIMUM_PLAYER_NUMBER:
            raise TerminalEventError(not_enough_players_event(Game.MINIMUM_PLAYER_NUMBER))

        engine_game = EngineGame(
            len(game.players), players_cards_number=Game.PLAYERS_CARD_NUMBER, rounds_number=Game.ROUND_NUMBER)
        engine_players = engine_game.players.copy()
        shuffle(engine_players)
        for i, server_player in enumerate(game.players):
            game.add_player(server_player, engine_players[i])

        game._engine_game = engine_game
        engine_game.start()
        game.define_cards_ids()

        game.event_handler.send_events()

    @ActionHandler.action()
    def send_lobby_chat_message(self, player, message):
        send_lobby_message(message, player, self)

    def as_dict(self):
        return {
            "players": [p.as_dict() for p in self.players],
            "games": [g.as_dict() for g in self.games],
            "maxChatMessageLength": MAX_MESSAGE_LENGTH
        }

    def send_logged_player(self, package, other_than=None):
        for player in self.logged_player:
            if player != other_than:
                player.send(package)

    def find_player(self, token):
        try:
            return [player for player in self.logged_player if player.session_token == token][0]
        except IndexError:
            raise TerminalEventError(no_such_logged_player_event(token))

    def find_game(self, game_name):
        try:
            return [game for game in self.games if game.name == game_name][0]
        except IndexError:
            raise NoSuchGame(game_name)

    def check_player_in_game(self, player):
        if games := [game for game in self.games if player in game.players]:
            raise TerminalEventError(player_already_in_a_game_event(player, games[0]))

    @staticmethod
    def get_handler(server, params):
        return server.lobby

    def logoff(self, player, send_disconnected_player=False):
        print(f"Logging off {player.name}")
        self.send_logged_player(disconnected_event(player), other_than=None if send_disconnected_player else player)
        player.logoff()

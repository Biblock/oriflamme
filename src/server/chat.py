from src.server.events import chat_message_too_long_event, TerminalEventError, lobby_chat_message_event, \
    game_chat_message_event

MAX_MESSAGE_LENGTH = 500


def validate(message):
    if len(message) > MAX_MESSAGE_LENGTH:
        raise TerminalEventError(chat_message_too_long_event())


def send_lobby_message(message, sender, lobby):
    validate(message)
    lobby.send_logged_player(lobby_chat_message_event(message, sender))


def send_game_message(message, sender, game):
    validate(message)
    for player in game.players:
        player.send(game_chat_message_event(message, sender, game))

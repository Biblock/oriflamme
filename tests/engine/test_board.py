import pytest

from src.engine.board import Board, BoardIterator
from src.engine.enums import Position

from src.engine.exceptions import NoSuchCardOnBoardError, CardAlreadyOnBoardError, CanNotPlaceOnOtherPlayerCardError, \
    CanNotMoveOnOtherCardError, CannotTargetCoveredCardError


@pytest.fixture
def board(events):
    return Board(events)


@pytest.fixture
def board_iterator(board: Board):
    return iter(board)


def test_board_iterator_empty(board_iterator: BoardIterator):
    with pytest.raises(StopIteration):
        next(board_iterator)


def test_board_place(board: Board, board_iterator: BoardIterator, p1_cards):
    board.place(card_1 := p1_cards(), Position.LEFT)
    assert next(board_iterator) == card_1

    board.place(card_2 := p1_cards(), Position.RIGHT)
    assert next(board_iterator) == card_2

    board.place(card_3 := p1_cards(), Position.ON, card_2)
    assert next(board_iterator) == card_3

    board.place(p1_cards(), Position.LEFT)
    board.place(card_4 := p1_cards(), Position.RIGHT)
    board.place(card_5 := p1_cards(), Position.RIGHT)
    assert next(board_iterator) == card_4
    assert next(board_iterator) == card_5

    with pytest.raises(CardAlreadyOnBoardError):
        board.place(card_1, Position.RIGHT)

    with pytest.raises(StopIteration):
        next(board_iterator)

    # Testing twice more just to be sure I could ask as much as a want, the iterator doesn't progress anymore
    with pytest.raises(StopIteration):
        next(board_iterator)

    with pytest.raises(StopIteration):
        next(board_iterator)


def test_board_place_on(board: Board, board_iterator: BoardIterator, p1_cards, p2_cards):
    board.place(card_1 := p1_cards(), Position.RIGHT)
    with pytest.raises(CanNotPlaceOnOtherPlayerCardError):
        board.place(p2_cards(), Position.ON, card_1)

    board.place(card_2 := p1_cards(), Position.ON, card_1)
    board.place(card_3 := p1_cards(), Position.RIGHT, card_2)
    board.place(card_4 := p1_cards(), Position.ON, card_3)
    board.place(card_5 := p1_cards(), Position.ON, card_4)

    assert next(board_iterator) == card_2
    assert next(board_iterator) == card_5

    with pytest.raises(StopIteration):
        next(board_iterator)


def test_target_covered_card(board: Board, p1_cards):
    board.place(card_1 := p1_cards(), Position.RIGHT)
    board.place(card_2 := p1_cards(), Position.RIGHT)
    board.place(card_3 := p1_cards(), Position.ON, card_2)
    board.place(card_4 := p1_cards(), Position.RIGHT)
    board.place(card_5 := p1_cards(), Position.ON, card_4)
    board.place(card_6 := p1_cards(), Position.ON, card_5)

    placement_failed_card = p1_cards()
    for card in card_2, card_4, card_5:
        with pytest.raises(CannotTargetCoveredCardError):
            board.place(placement_failed_card, Position.LEFT, card)

    for card in card_2, card_4, card_5:
        with pytest.raises(CannotTargetCoveredCardError):
            board.remove(card)

    for card in card_2, card_4, card_5:
        with pytest.raises(CannotTargetCoveredCardError):
            board.move(card, Position.LEFT, card_6)

    assert list(board) == [card_1, card_3, card_6]

    with pytest.raises(CannotTargetCoveredCardError):
        board.targeted_card_slot(card_5)
    board.remove(card_6)
    board.targeted_card_slot(card_5)

    assert list(board) == [card_1, card_3, card_5]

    with pytest.raises(CannotTargetCoveredCardError):
        board.targeted_card_slot(card_4)
    board.move(card_5, Position.RIGHT, card_5)
    board.targeted_card_slot(card_4)

    assert list(board) == [card_1, card_3, card_4, card_5]


def test_place_next_to(board: Board, board_iterator: BoardIterator, p1_cards, p2_cards):
    board.place(card_1 := p1_cards(), Position.RIGHT)
    board.place(card_2 := p1_cards(), Position.RIGHT)
    board.place(card_3 := p1_cards(), Position.RIGHT)

    board.place(card_4 := p1_cards(), Position.RIGHT, card_2)
    board.place(card_5 := p2_cards(), Position.LEFT, card_4)

    assert next(board_iterator) == card_1
    assert next(board_iterator) == card_2
    assert next(board_iterator) == card_5
    assert next(board_iterator) == card_4
    assert next(board_iterator) == card_3

    with pytest.raises(StopIteration):
        next(board_iterator)

    assert list(board) == [card_1, card_2, card_5, card_4, card_3]


def test_remove(board: Board, board_iterator: BoardIterator, p1_cards):
    board.place(card_1 := p1_cards(), Position.RIGHT)
    board.place(card_2 := p1_cards(), Position.RIGHT)
    board.place(card_3 := p1_cards(), Position.RIGHT)

    board.remove(card_1)
    assert next(board_iterator) == card_2
    board.remove(card_2)
    assert next(board_iterator) == card_3

    with pytest.raises(NoSuchCardOnBoardError):
        board.remove(p1_cards())

    assert list(board) == [card_3]
    assert board.extremities() == {card_3}


def test_remove_two_on_a_row(board: Board, board_iterator: BoardIterator, p1_cards):
    board.place(card_1 := p1_cards(), Position.RIGHT)
    board.place(card_2 := p1_cards(), Position.RIGHT)
    board.place(card_3 := p1_cards(), Position.RIGHT)

    board.remove(card_1)
    assert next(board_iterator) == card_2
    board.remove(card_3)
    board.remove(card_2)
    with pytest.raises(StopIteration):
        next(board_iterator)

    assert list(board) == []
    assert board.extremities() == set()


def test_remove_two_on_a_row_from_start(board: Board, board_iterator: BoardIterator, p1_cards):
    board.place(card_1 := p1_cards(), Position.RIGHT)
    board.place(card_2 := p1_cards(), Position.RIGHT)
    board.place(card_3 := p1_cards(), Position.RIGHT)

    assert next(board_iterator) == card_1
    board.remove(card_2)
    board.remove(card_1)
    assert next(board_iterator) == card_3

    assert list(board) == [card_3]
    assert board.extremities() == {card_3}


def test_remove_before(board: Board, board_iterator: BoardIterator, p1_cards):
    board.place(card_1 := p1_cards(), Position.RIGHT)
    assert next(board_iterator) == card_1
    board.place(card_2 := p1_cards(), Position.RIGHT)
    assert next(board_iterator) == card_2
    board.place(card_3 := p1_cards(), Position.RIGHT)
    assert next(board_iterator) == card_3
    board.place(card_4 := p1_cards(), Position.RIGHT)
    board.remove(card_2)
    assert next(board_iterator) == card_4

    assert list(board) == [card_1, card_3, card_4]


def test_remove_on(board: Board, board_iterator: BoardIterator, p1_cards):
    board.place(card_1 := p1_cards(), Position.RIGHT)
    board.place(card_2 := p1_cards(), Position.RIGHT)
    board.place(card_21 := p1_cards(), Position.ON, card_2)
    board.place(card_3 := p1_cards(), Position.RIGHT, card_21)
    board.place(card_31 := p1_cards(), Position.ON, card_3)
    board.place(card_99 := p1_cards(), Position.RIGHT)

    board.remove(card_21)
    board.remove(card_31)

    assert next(board_iterator) == card_1
    assert next(board_iterator) == card_2
    assert next(board_iterator) == card_3
    assert next(board_iterator) == card_99

    with pytest.raises(StopIteration):
        next(board_iterator)

    assert list(board) == [card_1, card_2, card_3, card_99]


def test_remove_on_while_on_stack(board: Board, board_iterator: BoardIterator, p1_cards):
    board.place(card_1 := p1_cards(), Position.RIGHT)
    board.place(card_2 := p1_cards(), Position.RIGHT)
    board.place(card_21 := p1_cards(), Position.ON, card_2)

    assert next(board_iterator) == card_1
    assert next(board_iterator) == card_21

    board.remove(card_21)

    assert next(board_iterator) == card_2

    with pytest.raises(StopIteration):
        next(board_iterator)

    assert list(board) == [card_1, card_2]


def test_movement_simple_right(board: Board, board_iterator: BoardIterator, p1_cards, p2_cards):
    board.place(card_1 := p1_cards(), Position.RIGHT)
    board.place(card_2 := p1_cards(), Position.RIGHT)
    board.place(card_3 := p2_cards(), Position.RIGHT)
    board.place(card_4 := p2_cards(), Position.RIGHT)
    board.place(card_5 := p2_cards(), Position.RIGHT)

    board.move(card_1, Position.RIGHT, card_5)
    assert next(board_iterator) == card_2
    assert next(board_iterator) == card_3
    assert next(board_iterator) == card_4
    assert next(board_iterator) == card_5
    assert next(board_iterator) == card_1

    with pytest.raises(StopIteration):
        next(board_iterator)

    assert list(board) == [card_2, card_3, card_4, card_5, card_1]
    assert board.extremities() == {card_2, card_1}


def test_movement_simple_left(board: Board, board_iterator: BoardIterator, p1_cards, p2_cards):
    board.place(card_1 := p1_cards(), Position.RIGHT)
    board.place(card_2 := p1_cards(), Position.RIGHT)
    board.place(card_3 := p2_cards(), Position.RIGHT)
    board.place(card_4 := p2_cards(), Position.RIGHT)
    board.place(card_5 := p2_cards(), Position.RIGHT)

    board.move(card_1, Position.LEFT, card_5)
    assert next(board_iterator) == card_2
    assert next(board_iterator) == card_3
    assert next(board_iterator) == card_4
    assert next(board_iterator) == card_1
    assert next(board_iterator) == card_5

    with pytest.raises(StopIteration):
        next(board_iterator)

    assert list(board) == [card_2, card_3, card_4, card_1, card_5]
    assert board.extremities() == {card_2, card_5}


def test_forbidden_movement_simple_on(board: Board, p1_cards, p2_cards):
    board.place(card_1 := p1_cards(), Position.RIGHT)
    board.place(card_2 := p2_cards(), Position.RIGHT)

    with pytest.raises(CanNotMoveOnOtherCardError):
        board.move(card_1, Position.ON, card_2)

    assert list(board) == [card_1, card_2]
    assert board.extremities() == {card_1, card_2}


def test_movement_jump_forward(board: Board, board_iterator: BoardIterator, p1_cards, p2_cards):
    board.place(card_1 := p1_cards(), Position.RIGHT)
    board.place(card_2 := p1_cards(), Position.RIGHT)
    board.place(card_3 := p2_cards(), Position.RIGHT)
    board.place(card_4 := p2_cards(), Position.RIGHT)
    board.place(card_5 := p2_cards(), Position.RIGHT)
    assert board.extremities() == {card_1, card_5}

    assert next(board_iterator) == card_1
    assert next(board_iterator) == card_2
    board.move(card_1, Position.RIGHT, card_2)
    assert next(board_iterator) == card_1
    assert next(board_iterator) == card_3
    assert next(board_iterator) == card_4
    assert next(board_iterator) == card_5

    with pytest.raises(StopIteration):
        next(board_iterator)

    assert list(board) == [card_2, card_1, card_3, card_4, card_5]
    assert board.extremities() == {card_2, card_5}


def test_movement_jump_backward(board: Board, board_iterator: BoardIterator, p1_cards, p2_cards):
    board.place(card_1 := p1_cards(), Position.RIGHT)
    board.place(card_2 := p1_cards(), Position.RIGHT)
    board.place(card_3 := p2_cards(), Position.RIGHT)
    board.place(card_4 := p2_cards(), Position.RIGHT)
    board.place(card_5 := p2_cards(), Position.RIGHT)
    assert board.extremities() == {card_1, card_5}

    assert next(board_iterator) == card_1
    assert next(board_iterator) == card_2

    board.move(card_3, Position.LEFT, card_1)
    assert next(board_iterator) == card_4
    assert next(board_iterator) == card_5

    with pytest.raises(StopIteration):
        next(board_iterator)

    assert list(board) == [card_3, card_1, card_2, card_4, card_5]
    assert board.extremities() == {card_3, card_5}


@pytest.mark.parametrize("position", [Position.LEFT, Position.RIGHT])
def test_movement_same_card(position, board: Board, p1_cards):
    board.place(card_1 := p1_cards(), Position.LEFT)
    assert board.extremities() == {card_1}
    assert list(board) == [card_1]
    board.move(card_1, position, card_1)
    assert list(board) == [card_1]
    assert board.extremities() == {card_1}


def test_movement_same_card_stack_right(board: Board, p1_cards):
    board.place(card_base := p1_cards(), Position.LEFT)
    board.place(card_1 := p1_cards(), Position.ON, card_base)
    assert board.extremities() == {card_1}
    assert list(board) == [card_1]
    board.move(card_1, Position.RIGHT, card_1)
    assert list(board) == [card_base, card_1]
    assert board.extremities() == {card_1, card_base}


def test_movement_same_card_huge_stack_right(board: Board, p1_cards):
    board.place(card_base := p1_cards(), Position.LEFT)
    board.place(card_base2 := p1_cards(), Position.ON, card_base)
    board.place(card_1 := p1_cards(), Position.ON, card_base2)
    assert board.extremities() == {card_1}
    assert list(board) == [card_1]
    board.move(card_1, Position.RIGHT, card_1)
    assert list(board) == [card_base2, card_1]
    assert board.extremities() == {card_1, card_base2}


def test_movement_same_card_stack_left(board: Board, p1_cards):
    board.place(card_base := p1_cards(), Position.LEFT)
    board.place(card_1 := p1_cards(), Position.ON, card_base)
    assert board.extremities() == {card_1}
    assert list(board) == [card_1]
    board.move(card_1, Position.LEFT, card_1)
    assert list(board) == [card_1, card_base]
    assert board.extremities() == {card_1, card_base}


def test_movement_same_card_huge_stack_left(board: Board, p1_cards):
    board.place(card_base := p1_cards(), Position.LEFT)
    board.place(card_base2 := p1_cards(), Position.ON, card_base)
    board.place(card_1 := p1_cards(), Position.ON, card_base2)
    assert board.extremities() == {card_1}
    assert list(board) == [card_1]
    board.move(card_1, Position.LEFT, card_1)
    assert list(board) == [card_1, card_base2]
    assert board.extremities() == {card_1, card_base2}


def test_get_stacks_and_cards(board: Board, p1_cards, p2_cards):
    board.place(card_1 := p1_cards(), Position.LEFT)
    board.place(card_2 := p2_cards(), Position.RIGHT)
    board.place(card_6 := p1_cards(), Position.RIGHT)
    board.place(card_8 := p2_cards(), Position.RIGHT)
    board.place(card_7 := p1_cards(), Position.LEFT, card_8)
    board.place(card_3 := p2_cards(), Position.RIGHT, card_2)
    board.place(card_31 := p2_cards(), Position.ON, card_3)
    board.place(card_32 := p2_cards(), Position.ON, card_31)
    board.place(card_4 := p1_cards(), Position.RIGHT, card_32)
    board.place(card_5 := p1_cards(), Position.LEFT, card_6)
    board.place(card_51 := p1_cards(), Position.ON, card_5)

    assert board.stacks == [
        [card_1],
        [card_2],
        [card_3, card_31, card_32],
        [card_4],
        [card_5, card_51],
        [card_6],
        [card_7],
        [card_8]
    ]
    assert board.cards == {
        card_1,
        card_2,
        card_3, card_31, card_32,
        card_4,
        card_5, card_51,
        card_6,
        card_7,
        card_8,
    }


def test_adjacent_cards(board, p1_cards, p2_cards):
    board.place(card_1 := p1_cards(), Position.LEFT)
    board.place(card_2 := p2_cards(), Position.RIGHT)
    board.place(card_6 := p1_cards(), Position.RIGHT)
    board.place(card_8 := p2_cards(), Position.RIGHT)
    board.place(card_7 := p1_cards(), Position.LEFT, card_8)
    board.place(card_3 := p2_cards(), Position.RIGHT, card_2)
    board.place(card_31 := p2_cards(), Position.ON, card_3)
    board.place(card_32 := p2_cards(), Position.ON, card_31)
    board.place(card_4 := p1_cards(), Position.RIGHT, card_32)
    board.place(card_5 := p1_cards(), Position.LEFT, card_6)
    board.place(card_51 := p1_cards(), Position.ON, card_5)

    assert board.adjacent_cards(card_7) == {card_6, card_8}
    assert board.adjacent_cards(card_2) == {card_1, card_32}
    assert board.adjacent_cards(card_4) == {card_32, card_51}
    assert board.adjacent_cards(card_6) == {card_51, card_7}
    assert board.adjacent_cards(card_1) == {card_2}
    assert board.adjacent_cards(card_8) == {card_7}

    with pytest.raises(NoSuchCardOnBoardError):
        board.adjacent_cards(p2_cards())


def test_extremities_cards(board, p1_cards, p2_cards):
    board.place(card_1 := p1_cards(), Position.LEFT)
    board.place(card_2 := p2_cards(), Position.RIGHT)
    board.place(card_6 := p1_cards(), Position.RIGHT)
    board.place(card_7 := p2_cards(), Position.RIGHT)
    board.place(card_3 := p2_cards(), Position.RIGHT, card_2)
    board.place(card_31 := p2_cards(), Position.ON, card_3)
    board.place(card_32 := p2_cards(), Position.ON, card_31)
    board.place(p1_cards(), Position.RIGHT, card_32)
    board.place(card_5 := p1_cards(), Position.LEFT, card_6)
    board.place(p1_cards(), Position.ON, card_5)

    assert board.extremities() == {card_7, card_1}

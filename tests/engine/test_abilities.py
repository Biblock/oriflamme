import pytest

from src.engine.ability import Changeforme, Soldat
from src.engine.enums import CardName, Position
from src.engine.event_manager import StolenPlayerEvent, MurderedCardEvent, CardDiscardedEvent, \
    PlayerInfluenceUpdateEvent
from src.engine.exceptions import InvalidUserInputError, NeedUserInputError
from src.engine.user_input import ChoosePlayerUserInput, ChooseCardUserInput, ChooseMovementUserInput


@pytest.mark.parametrize(
    "ability, multiplier",
    [(CardName.ARCHER, 1), (CardName.SOLDAT, 1), (CardName.ESPION, 1), (CardName.HERITIER, 1),
     (CardName.ASSASSINAT, 1), (CardName.DECRET_ROYAL, 1), (CardName.SEIGNEUR, 1),
     (CardName.CHANGEFORME, 1),
     (CardName.EMBUSCADE, None),
     (CardName.COMPLOT, 2)])
def test_abilities_reveal(ability, multiplier, p1_cards, player_1):
    card = p1_cards(ability)
    card.influence = 2
    assert player_1.influence == 1
    card.ability.reveal(card)
    if ability == CardName.EMBUSCADE:
        assert player_1.influence == 2
    else:
        assert player_1.influence == 1 + card.influence * multiplier


@pytest.mark.parametrize(
    "ability",
    [CardName.ARCHER, CardName.SOLDAT, CardName.ESPION, CardName.ASSASSINAT,
     CardName.DECRET_ROYAL, CardName.CHANGEFORME])
def test_input_abilities(ability, board, player_1, p1_cards):
    player_1.play_card_name(board, (card := p1_cards(ability)).name, Position.RIGHT)
    with pytest.raises(NeedUserInputError):
        card.validate(board, None)


def test_heritier(p1_cards, p2_cards, board, player_1, player_2):
    heritier = p1_cards(CardName.HERITIER)
    p2_heritier = p2_cards(CardName.HERITIER)
    player_1.play_card_name(board, heritier.name, Position.RIGHT)
    assert player_1.influence == 1

    heritier.activate(board)
    assert player_1.influence == 3

    player_2.play_card_name(board, p2_heritier.name, Position.RIGHT)
    heritier.activate(board)
    assert player_1.influence == 5

    p2_heritier.revealed = True
    heritier.activate(board)
    assert player_1.influence == 5


def test_seigneur(p1_cards, p2_cards, board, player_1, player_2):
    seigneur = p1_cards(CardName.SEIGNEUR)
    player_1.play_card_name(board, seigneur.name, Position.RIGHT)
    assert player_1.influence == 1

    seigneur.activate(board)
    assert player_1.influence == 2

    player_1.play_card_name(board, (left_card := p1_cards()).name, Position.LEFT)
    seigneur.activate(board)
    assert not left_card.revealed
    assert player_1.influence == 4

    left_card.revealed = True
    seigneur.activate(board)
    assert left_card.revealed
    assert player_1.influence == 6

    player_1.play_card_name(board, (right_card := p1_cards()).name, Position.RIGHT)
    seigneur.activate(board)
    assert player_1.influence == 9

    player_1.play_card_name(board, p1_cards().name, Position.RIGHT)
    seigneur.activate(board)
    assert player_1.influence == 12

    player_1.play_card_name(board, p1_cards().name, Position.ON, right_card)
    seigneur.activate(board)
    assert player_1.influence == 15

    player_1.play_card_name(board, p1_cards().name, Position.RIGHT)
    seigneur.activate(board)
    assert player_1.influence == 18

    player_2.play_card_name(board, p2_cards().name, Position.RIGHT, seigneur)
    seigneur.activate(board)
    assert player_1.influence == 20

    player_2.play_card_name(board, p2_cards().name, Position.LEFT, seigneur)
    seigneur.activate(board)
    assert player_1.influence == 21


def test_espion(p1_cards, p2_cards, board, player_1, player_2, events):
    espion = p1_cards(CardName.ESPION)
    player_1.play_card_name(board, espion.name, Position.RIGHT)
    player_1.influence = player_2.influence = 10
    assert player_1.influence == 10
    assert player_2.influence == 10

    player_2_choice = ChoosePlayerUserInput(player_2)
    with pytest.raises(InvalidUserInputError):
        espion.validate(board, player_2_choice)

    player_2.play_card_name(board, (p2_card := p2_cards()).name, Position.RIGHT)
    assert not p2_card.revealed
    espion.validate(board, player_2_choice)
    espion.activate(board, player_2_choice)
    assert player_1.influence == 11
    assert player_2.influence == 9
    assert isinstance(event := events[-3], StolenPlayerEvent)
    assert event.choosing_card == espion
    assert event.target_player == player_2
    assert event.loss == 1

    p2_card.revealed = True
    espion.validate(board, player_2_choice)
    espion.activate(board, player_2_choice)
    assert player_1.influence == 12
    assert player_2.influence == 8

    player_1.play_card_name(board, p1_cards().name, Position.RIGHT, espion)
    with pytest.raises(InvalidUserInputError):
        espion.validate(board, player_2_choice)

    self_player_choice = ChoosePlayerUserInput(player_1)
    espion.validate(board, self_player_choice)
    espion.activate(board, self_player_choice)
    assert player_1.influence == 12
    assert player_2.influence == 8


def test_complot(board, player_1, p1_cards):
    player_1.play_card_name(board, (complot := p1_cards(CardName.COMPLOT)).name, Position.RIGHT)
    complot.influence = 5

    assert player_1.influence == 1
    assert complot.influence == 5
    complot.reveal()
    assert player_1.influence == 11

    assert complot in board.cards
    complot.activate(board)
    assert complot not in board.cards


def test_soldat(events, board, player_1, player_2, p1_cards, p2_cards):
    player_2.play_card_name(board, (adj_left_card := p2_cards(CardName.HERITIER)).name, Position.RIGHT)
    player_1.play_card_name(board, (soldat := p1_cards(CardName.SOLDAT)).name, Position.RIGHT)
    player_2.play_card_name(board, (base_card := p2_cards(CardName.SEIGNEUR)).name, Position.RIGHT)
    player_2.play_card_name(board, (adj_right_card := p2_cards(CardName.DECRET_ROYAL)).name, Position.ON, base_card)
    player_2.play_card_name(board, (not_adj_card := p2_cards(CardName.CHANGEFORME)).name, Position.RIGHT)

    assert not adj_left_card.revealed

    soldat.validate(board, ChooseCardUserInput(adj_left_card))

    adj_left_card.revealed = True
    adj_right_card.revealed = True

    with pytest.raises(InvalidUserInputError):
        soldat.validate(board, ChooseCardUserInput(not_adj_card))

    with pytest.raises(InvalidUserInputError):
        soldat.validate(board, ChooseCardUserInput(base_card))

    assert soldat in board.cards
    assert player_1.influence == 1
    assert adj_left_card in board.cards
    assert adj_right_card in board.cards
    soldat.validate(board, ChooseCardUserInput(adj_right_card))
    soldat.validate(board, ChooseCardUserInput(adj_left_card))
    soldat.activate(board, ChooseCardUserInput(adj_right_card))
    assert isinstance(events[-3], CardDiscardedEvent)
    assert isinstance(events[-2], MurderedCardEvent)
    assert isinstance(events[-1], PlayerInfluenceUpdateEvent)
    assert soldat in board.cards
    assert adj_right_card not in board.cards
    assert player_1.influence == 2

    soldat.activate(board, ChooseCardUserInput(adj_left_card))
    assert adj_left_card not in board.cards
    assert player_1.influence == 3

    soldat.validate(board, ChooseCardUserInput(soldat))
    soldat.activate(board, ChooseCardUserInput(soldat))

    assert soldat not in board.cards
    assert player_1.influence == 4


@pytest.mark.parametrize("target_card_index", list(range(4)))
def test_assassinat(target_card_index, board, player_1, player_2, p1_cards, p2_cards):
    player_2.play_card_name(board, (adj_left_card := p2_cards(CardName.HERITIER)).name, Position.RIGHT)
    player_1.play_card_name(board, (assassinat := p1_cards(CardName.ASSASSINAT)).name, Position.RIGHT)
    player_2.play_card_name(board, (base_card := p2_cards(CardName.SEIGNEUR)).name, Position.RIGHT)
    player_2.play_card_name(board, (adj_right_card := p2_cards(CardName.DECRET_ROYAL)).name, Position.ON, base_card)
    player_2.play_card_name(board, (not_adj_card := p2_cards(CardName.CHANGEFORME)).name, Position.RIGHT)

    target_cards = [adj_left_card, adj_right_card, not_adj_card, assassinat]
    target_card = target_cards[target_card_index]
    with pytest.raises(InvalidUserInputError):
        assassinat.validate(board, ChooseCardUserInput(base_card))

    assert player_1.influence == 1

    old_influence = player_1.influence
    assert target_card in board.cards

    assassinat.validate(board, ChooseCardUserInput(target_card))
    assassinat.activate(board, ChooseCardUserInput(target_card))

    assert target_card not in board.cards
    assert assassinat not in board.cards
    assert player_1.influence == old_influence + 1


def test_archer(board, player_1, player_2, p1_cards, p2_cards):
    player_2.play_card_name(board, (extreme_left_card := p2_cards(CardName.HERITIER)).name, Position.RIGHT)
    player_1.play_card_name(board, (archer := p1_cards(CardName.ARCHER)).name, Position.RIGHT)
    player_2.play_card_name(board, (base_card := p2_cards(CardName.SEIGNEUR)).name, Position.RIGHT)
    player_2.play_card_name(board, (adj_right_card := p2_cards(CardName.DECRET_ROYAL)).name, Position.ON, base_card)
    player_2.play_card_name(board, (extreme_right_card := p2_cards(CardName.CHANGEFORME)).name, Position.RIGHT)

    assert not extreme_left_card.revealed
    archer.validate(board, ChooseCardUserInput(extreme_left_card))
    extreme_left_card.revealed = True
    assert extreme_left_card.revealed
    archer.validate(board, ChooseCardUserInput(extreme_left_card))

    with pytest.raises(InvalidUserInputError):
        archer.validate(board, ChooseCardUserInput(base_card))

    with pytest.raises(InvalidUserInputError):
        archer.validate(board, ChooseCardUserInput(adj_right_card))

    archer.validate(board, ChooseCardUserInput(extreme_right_card))
    archer.activate(board, ChooseCardUserInput(extreme_right_card))

    assert archer in board.cards
    assert extreme_right_card not in board.cards
    assert board.extremities() == {extreme_left_card, adj_right_card}

    archer.validate(board, ChooseCardUserInput(adj_right_card))
    archer.activate(board, ChooseCardUserInput(adj_right_card))
    assert adj_right_card not in board.cards
    assert board.extremities() == {extreme_left_card, base_card}

    archer.validate(board, ChooseCardUserInput(archer))
    archer.activate(board, ChooseCardUserInput(archer))
    assert archer not in board.cards


@pytest.mark.parametrize("murderer_kind", [CardName.SOLDAT, CardName.ARCHER, CardName.ASSASSINAT])
def test_embuscade_other_player(murderer_kind, board, player_1, player_2, p1_cards, p2_cards):
    player_1.play_card_name(board, (murderer := p1_cards(murderer_kind)).name, Position.RIGHT)
    player_2.play_card_name(board, (p2_embuscade := p2_cards(CardName.EMBUSCADE)).name, Position.RIGHT)

    assert player_1.influence == 1
    assert player_2.influence == 1

    murderer.activate(board, ChooseCardUserInput(p2_embuscade))
    assert murderer not in board.cards
    assert p2_embuscade not in board.cards
    assert player_1.influence == 2
    assert player_2.influence == 5


@pytest.mark.parametrize("murderer_kind", [CardName.SOLDAT, CardName.ARCHER, CardName.ASSASSINAT])
def test_embuscade_same_player(murderer_kind, board, player_1, p1_cards):
    player_1.play_card_name(board, (murderer := p1_cards(murderer_kind)).name, Position.RIGHT)
    player_1.play_card_name(board, (p1_embuscade := p1_cards(CardName.EMBUSCADE)).name, Position.RIGHT)

    assert player_1.influence == 1
    murderer.activate(board, ChooseCardUserInput(p1_embuscade))
    if murderer_kind == CardName.ASSASSINAT:
        assert murderer not in board.cards
    else:
        assert murderer in board.cards
    assert p1_embuscade not in board.cards
    assert player_1.influence == 3


def test_activate_embuscade(board, player_1, p1_cards):
    p1_embuscade = p1_cards(CardName.EMBUSCADE)
    assert player_1.influence == 1
    p1_embuscade.activate(board)
    assert player_1.influence == 1
    assert p1_embuscade not in board.cards


def test_decret_royal_move_self(board, player_1, p1_cards):
    player_1.play_card_name(board, (decret_royal := p1_cards(CardName.DECRET_ROYAL)).name, Position.RIGHT)
    decret_royal.validate(board, ChooseMovementUserInput(decret_royal, Position.LEFT, decret_royal))
    decret_royal.activate(board, ChooseMovementUserInput(decret_royal, Position.LEFT, decret_royal))

    assert board.cards == set()


def test_decret_royal(board, player_1, p1_cards):
    player_1.play_card_name(board, (decret_royal := p1_cards(CardName.DECRET_ROYAL)).name, Position.RIGHT)
    player_1.play_card_name(board, (base_card := p1_cards()).name, Position.RIGHT)
    player_1.play_card_name(board, (other_card := p1_cards()).name, Position.ON, base_card)

    with pytest.raises(InvalidUserInputError):
        decret_royal.validate(board, ChooseMovementUserInput(other_card, Position.ON, decret_royal))

    with pytest.raises(InvalidUserInputError):
        decret_royal.validate(board, ChooseMovementUserInput(base_card, Position.LEFT, other_card))

    with pytest.raises(InvalidUserInputError):
        decret_royal.validate(board, ChooseMovementUserInput(other_card, Position.LEFT, base_card))

    assert board.stacks == [[decret_royal], [base_card, other_card]]
    decret_royal.validate(board, ChooseMovementUserInput(other_card, Position.RIGHT, other_card))
    decret_royal.activate(board, ChooseMovementUserInput(other_card, Position.RIGHT, other_card))
    assert board.stacks == [[base_card], [other_card]]


def test_changeforme(board, player_1, p1_cards, player_2, p2_cards):
    player_1.play_card_name(board, (changeforme := p1_cards(CardName.CHANGEFORME)).name, Position.RIGHT)
    player_2.play_card_name(board, (heritier := p2_cards(CardName.HERITIER)).name, Position.RIGHT)
    player_2.play_card_name(board, (non_adj_card := p2_cards(CardName.SEIGNEUR)).name, Position.RIGHT)

    non_adj_card.revealed = True
    with pytest.raises(InvalidUserInputError):
        changeforme.validate(board, ChooseCardUserInput(non_adj_card))
    with pytest.raises(InvalidUserInputError):
        changeforme.validate(board, ChooseCardUserInput(heritier))

    heritier.revealed = True
    assert changeforme.ability == Changeforme
    assert player_1.influence == 1
    changeforme.validate(board, ChooseCardUserInput(heritier))
    changeforme.activate(board, ChooseCardUserInput(heritier))
    assert player_1.influence == 3
    assert changeforme.ability == Changeforme

    changeforme.validate(board, ChooseCardUserInput(changeforme))
    changeforme.activate(board, ChooseCardUserInput(changeforme))
    assert changeforme.ability == Changeforme

    player_1.play_card_name(board, (soldat := p1_cards(CardName.SOLDAT)).name, Position.RIGHT, changeforme)
    soldat.revealed = True
    changeforme.validate(board, ChooseCardUserInput(soldat))
    with pytest.raises(NeedUserInputError):
        changeforme.activate(board, ChooseCardUserInput(soldat))
    assert isinstance(changeforme.ability, Changeforme.ChangeFormCopiedAbility)
    assert changeforme.ability.copied_ability == Soldat

    with pytest.raises(InvalidUserInputError):
        changeforme.validate(board, ChooseCardUserInput(heritier))

    assert soldat in board.cards
    changeforme.validate(board, ChooseCardUserInput(soldat))
    changeforme.activate(board, ChooseCardUserInput(soldat))
    assert changeforme.ability == Changeforme
    assert soldat not in board.cards
    assert player_1.influence == 4

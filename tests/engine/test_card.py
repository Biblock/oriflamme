import pytest


def _p_cards(player):
    cards = list(player.hand.copy())

    def _card(name=None):
        if name is None:
            return cards.pop(0)
        else:
            card = [card for card in player.hand if card.name == name][0]
            cards.remove(card)
            return card

    return _card


@pytest.fixture
def p1_cards(player_1):
    return _p_cards(player_1)


@pytest.fixture
def p2_cards(player_2):
    return _p_cards(player_2)


@pytest.fixture
def p3_cards(player_3):
    return _p_cards(player_3)


@pytest.fixture
def p_cards():
    return _p_cards

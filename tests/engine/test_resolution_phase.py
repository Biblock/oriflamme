import pytest

from src.engine.enums import RevealChoice, Position, CardName
from src.engine.event_manager import PromptPlayerEvent, RevealedCardEvent
from src.engine.exceptions import PhaseNotStartedError, PhaseOverError, NotPlayerTurnError, NeedUserInputError, \
    InvalidUserInputError
from src.engine.phase import ResolutionPhase
from src.engine.user_input import RevealUserInput, ChoosePlayerUserInput, ChooseCardUserInput


@pytest.fixture
def resolution_phase(board, p1_cards, p2_cards, events):
    board.place(p1_cards(CardName.HERITIER), Position.LEFT)
    board.place(p2_cards(CardName.SEIGNEUR), Position.RIGHT)
    board.place(p1_cards(CardName.ESPION), Position.RIGHT)
    board.place(p1_cards(CardName.SEIGNEUR), Position.RIGHT)
    return ResolutionPhase(events, board)


def test_resolution_phase(resolution_phase, board, player_1, player_2):
    # I could use the board iterator, but without that's another test for board.stacks
    p1_heritier = board.stacks[0][-1]
    p2_seigneur = board.stacks[1][-1]
    p1_espion = board.stacks[2][-1]
    p1_seigneur = board.stacks[3][-1]
    p1_seigneur.revealed = True

    assert not resolution_phase.is_started
    with pytest.raises(NeedUserInputError):
        resolution_phase.start()
    assert resolution_phase.is_started

    assert resolution_phase.active_card == p1_heritier
    assert not p1_heritier.revealed
    with pytest.raises(NeedUserInputError):
        resolution_phase.proceed(player_1, RevealUserInput(RevealChoice.REVEAL))

    assert p1_heritier.revealed
    assert player_1.influence == 3
    assert player_2.influence == 1

    assert resolution_phase.active_card == p2_seigneur
    assert not p2_seigneur.revealed
    with pytest.raises(NeedUserInputError):
        resolution_phase.proceed(player_2, RevealUserInput(RevealChoice.REVEAL))
    assert p2_seigneur.revealed
    assert player_2.influence == 2

    assert resolution_phase.active_card == p1_espion
    assert not p1_espion.revealed
    with pytest.raises(NeedUserInputError):
        resolution_phase.proceed(player_1, RevealUserInput(RevealChoice.REVEAL))
    assert p1_espion.revealed
    assert isinstance(resolution_phase.events[-1], PromptPlayerEvent)
    assert isinstance(resolution_phase.events[-2], RevealedCardEvent)

    assert resolution_phase.active_card == p1_espion
    with pytest.raises(PhaseOverError):
        resolution_phase.proceed(player_1, ChoosePlayerUserInput(player_2))

    assert player_1.influence == 6
    assert player_2.influence == 1


def test_wrong_player(resolution_phase, player_2):
    with pytest.raises(NeedUserInputError):
        resolution_phase.start()
    with pytest.raises(NotPlayerTurnError):
        resolution_phase.proceed(player_2, RevealUserInput(RevealChoice.REVEAL))


def test_not_started_phase(resolution_phase):
    with pytest.raises(PhaseNotStartedError):
        resolution_phase.proceed()
    with pytest.raises(NeedUserInputError):
        resolution_phase.start()


def test_empty_board(events, board):
    resolution_phase = ResolutionPhase(events, board)

    with pytest.raises(PhaseOverError):
        resolution_phase.start()


def test_revealed_and_no_input_board(events, board, p1_cards, p2_cards):
    board.place(p2_heritier := p2_cards(CardName.HERITIER), Position.RIGHT)
    board.place(p2_seigneur := p2_cards(CardName.SEIGNEUR), Position.ON, p2_heritier)
    board.place(p1_heritier := p1_cards(CardName.HERITIER), Position.LEFT)
    p1_heritier.revealed = True
    p2_seigneur.revealed = True
    p2_heritier.revealed = True
    resolution_phase = ResolutionPhase(events, board)

    assert p1_heritier.player != p2_seigneur.player
    assert p1_heritier.player.influence == p2_seigneur.player.influence == 1

    with pytest.raises(PhaseOverError):
        resolution_phase.start()

    assert p1_heritier.player.influence == 3
    assert p2_seigneur.player.influence == 2


def test_wrong_card_input(events, board, player_1, p1_cards, player_3, p2_cards):
    board.place(espion := p1_cards(CardName.ESPION), Position.RIGHT)
    board.place(p2_card := p2_cards(), Position.RIGHT)
    assert not p2_card.revealed
    espion.revealed = True
    resolution_phase = ResolutionPhase(events, board)
    with pytest.raises(NeedUserInputError) as err:
        resolution_phase.start()
    assert isinstance(err.value.event, PromptPlayerEvent)
    assert resolution_phase.active_card == espion

    with pytest.raises(InvalidUserInputError):
        resolution_phase.proceed(player_1, ChooseCardUserInput(p2_card))
    assert resolution_phase.active_card == espion

    with pytest.raises(InvalidUserInputError):
        resolution_phase.proceed(player_1, ChoosePlayerUserInput(player_3))
    assert resolution_phase.active_card == espion


@pytest.mark.parametrize("only_card", [True, False])
def test_discarded_cards(only_card, events, board, player_1, p1_cards):
    resolution_phase = ResolutionPhase(events, board)
    player_1.play_card_name(board, (complot := p1_cards(CardName.COMPLOT)).name, Position.RIGHT)
    other_card = None
    if not only_card:
        player_1.play_card_name(board, (other_card := p1_cards(CardName.HERITIER)).name, Position.RIGHT)

    assert board.cards == {complot} if only_card else {complot, other_card}
    with pytest.raises(NeedUserInputError):
        resolution_phase.start()

    expected_pause = PhaseOverError if only_card else NeedUserInputError
    with pytest.raises(expected_pause):
        resolution_phase.proceed(player_1, RevealUserInput(RevealChoice.REVEAL))
    if not only_card:
        assert resolution_phase.active_card == other_card
    assert board.cards == set() if only_card else {other_card}

    if not only_card:
        with pytest.raises(PhaseOverError):
            resolution_phase.proceed(player_1, RevealUserInput(RevealChoice.REVEAL))


def test_self_murder(events, board, player_1, player_2, p1_cards, p2_cards):
    resolution_phase = ResolutionPhase(events, board)
    player_1.play_card_name(board, (soldat := p1_cards(CardName.SOLDAT)).name, Position.RIGHT)
    player_2.play_card_name(board, (next_card := p2_cards(CardName.HERITIER)).name, Position.RIGHT)
    soldat.revealed = True
    with pytest.raises(NeedUserInputError):
        resolution_phase.start()

    assert soldat in board.cards
    with pytest.raises(NeedUserInputError):
        resolution_phase.proceed(player_1, ChooseCardUserInput(soldat))
    assert soldat not in board.cards
    assert next_card in board.cards
    assert resolution_phase.active_card == next_card


@pytest.mark.parametrize("murderer_type", [CardName.SOLDAT, CardName.ASSASSINAT])
def test_murder_next_card(murderer_type, events, board, player_1, player_2, p1_cards, p2_cards):
    resolution_phase = ResolutionPhase(events, board)
    player_1.play_card_name(board, (murderer := p1_cards(murderer_type)).name, Position.RIGHT)
    player_2.play_card_name(board, (target_card := p2_cards(CardName.HERITIER)).name, Position.RIGHT)
    player_2.play_card_name(board, (next_card := p2_cards(CardName.SEIGNEUR)).name, Position.RIGHT)
    murderer.revealed = True
    target_card.revealed = True
    with pytest.raises(NeedUserInputError):
        resolution_phase.start()

    assert murderer in board.cards
    with pytest.raises(NeedUserInputError):
        resolution_phase.proceed(player_1, ChooseCardUserInput(target_card))
    assert target_card not in board.cards
    if murderer_type == CardName.ASSASSINAT:
        assert murderer not in board.cards
    else:
        assert murderer in board.cards
    assert next_card in board.cards
    assert resolution_phase.active_card == next_card


def test_assassinat_next_card(events, board, player_1, player_2, p1_cards, p2_cards):
    resolution_phase = ResolutionPhase(events, board)
    player_1.play_card_name(board, (assassinat := p1_cards(CardName.ASSASSINAT)).name, Position.RIGHT)
    player_2.play_card_name(board, (target_card := p2_cards(CardName.HERITIER)).name, Position.RIGHT)
    player_2.play_card_name(board, (next_card := p2_cards(CardName.SEIGNEUR)).name, Position.RIGHT)
    assassinat.revealed = True
    target_card.revealed = True
    with pytest.raises(NeedUserInputError):
        resolution_phase.start()

    assert assassinat in board.cards
    with pytest.raises(NeedUserInputError):
        resolution_phase.proceed(player_1, ChooseCardUserInput(target_card))
    assert target_card not in board.cards
    assert assassinat not in board.cards
    assert next_card in board.cards
    assert resolution_phase.active_card == next_card


def test_murder_embuscade_only_cards(events, board, player_1, player_2, p1_cards, p2_cards):
    resolution_phase = ResolutionPhase(events, board)
    player_1.play_card_name(board, (soldat := p1_cards(CardName.SOLDAT)).name, Position.RIGHT)
    player_2.play_card_name(board, (embuscade := p2_cards(CardName.EMBUSCADE)).name, Position.RIGHT)

    with pytest.raises(NeedUserInputError):
        resolution_phase.start()
    with pytest.raises(NeedUserInputError):
        resolution_phase.proceed(player_1, RevealUserInput(RevealChoice.REVEAL))
    with pytest.raises(PhaseOverError):
        resolution_phase.proceed(player_1, ChooseCardUserInput(embuscade))


def test_changeforme_with_no_input(events, board, player_1, p1_cards):
    resolution_phase = ResolutionPhase(events, board)
    player_1.play_card_name(board, (changeforme := p1_cards(CardName.CHANGEFORME)).name, Position.RIGHT)
    player_1.play_card_name(board, (seigneur := p1_cards(CardName.SEIGNEUR)).name, Position.RIGHT)
    changeforme.revealed = True
    seigneur.revealed = True

    with pytest.raises(NeedUserInputError):
        resolution_phase.start()

    assert player_1.influence == 1
    with pytest.raises(PhaseOverError):
        resolution_phase.proceed(player_1, ChooseCardUserInput(seigneur))
    assert player_1.influence == 5


def test_changeforme_with_input(events, board, player_1, p1_cards, player_2, p2_cards):
    resolution_phase = ResolutionPhase(events, board)
    player_2.play_card_name(board, (dead_man := p2_cards(CardName.SEIGNEUR)).name, Position.RIGHT)
    player_1.play_card_name(board, (changeforme := p1_cards(CardName.CHANGEFORME)).name, Position.RIGHT)
    player_2.play_card_name(board, (espion := p2_cards(CardName.ESPION)).name, Position.RIGHT)
    changeforme.revealed = True
    dead_man.revealed = True
    espion.revealed = True

    with pytest.raises(NeedUserInputError):
        resolution_phase.start()

    with pytest.raises(NeedUserInputError):
        resolution_phase.proceed(player_1, ChooseCardUserInput(espion))
    assert resolution_phase.active_card == changeforme
    assert player_1.influence == 1
    assert player_2.influence == 2
    with pytest.raises(NeedUserInputError):
        resolution_phase.proceed(player_1, ChoosePlayerUserInput(player_2))
    assert player_1.influence == 2
    assert player_2.influence == 1
    assert resolution_phase.active_card == espion
    with pytest.raises(PhaseOverError):
        resolution_phase.proceed(player_2, ChoosePlayerUserInput(player_1))
    assert player_1.influence == 1
    assert player_2.influence == 2

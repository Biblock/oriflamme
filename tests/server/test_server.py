import json

import pytest

from src.server.events import parameter_missing_event, invalid_json_syntax_event, no_such_action_event, \
    no_such_logged_player_event
from src.server.handler import ActionHandler
from src.server.server import Server


@pytest.fixture
def websocket(mocker):
    return mocker.Mock()


@pytest.fixture
def websockets(mocker):
    def ws_provider(number=1):
        return [mocker.Mock() for _ in range(number)]

    return ws_provider


def test_json_input(websockets):
    server = Server(None, None)
    w1, w2 = websockets(2)
    server.process_message(w1, "Not json")
    w1.send.assert_called_with(invalid_json_syntax_event("Not json"), None)

    server.process_message(w1, json.dumps({
        "not_action": None
    }))
    w1.send.assert_called_with(parameter_missing_event("action"), None)
    w1.reset_mock()

    assert not server.lobby.players
    server.process_message(w1, json.dumps({
        "action": "login",
        "name": "Jim"
    }))
    jim = server.lobby.players[0]
    assert jim.name == "Jim"

    server.process_message(w1, json.dumps({
        "action": "login",
        "What kind of bear is best": "Black bear"
    }))
    w1.send.assert_called_with(parameter_missing_event("name", "login"), None)
    w1.reset_mock()

    server.process_message(w1, json.dumps({
        "action": "gabagool",
        "If the salad is on top": "I send it back"
    }))
    w1.send.assert_called_with(no_such_action_event("gabagool"), None)
    w1.reset_mock()

    # Field session_token missing
    server.process_message(w2, json.dumps({
        "action": "create_game",
        "game_name": "The Office"

    }))
    w2.send.assert_called_with(parameter_missing_event("session_token", "create_game"), None)
    w2.reset_mock()

    # NO_SUCH_LOGGED_PLAYER
    server.process_message(w2, json.dumps({
        "action": "create_game",
        "session_token": "TAGAZOO",
        "game_name": "The Office"
    }))
    w2.send.assert_called_with(no_such_logged_player_event("TAGAZOO"), None)
    w2.reset_mock()

    jim.logoff()

    server.process_message(w2, json.dumps({
        "action": "create_game",
        "session_token": jim.session_token,
        "game_name": "The Office"
    }))
    w2.send.assert_called_with(no_such_logged_player_event(jim.session_token), None)
    w2.reset_mock()


def test_game_input(lobby_with_game, mocker):
    lobby = lobby_with_game
    game = lobby.games[0]
    player = game.players[0]
    ws = player.ws_wrapper
    action_name = "play_card"
    action = ActionHandler.ACTIONS["Game"][action_name]
    action_function = action["function"] = mocker.Mock()
    lobby.server.process_message(ws, json.dumps({
        "action": action_name,
        "game_name": game.name,
        "session_token": player.session_token,
        "card_name": "Blue eyes white dragon, bitch",
        "position_name": "LEFT"
    }))

    action_function.assert_any_call(game, player, card_name="Blue eyes white dragon, bitch", position_name="LEFT")

from itertools import combinations

from src.server.player import Player


def test_as_dict():
    dwight = Player("Dwight")
    assert dwight.as_dict() == {
        "id": dwight.id,
        "name": "Dwight",
        "logged": False
    }


def test_eq(websockets):
    w1, w2, w3 = websockets(3)
    p1 = Player("Jim")
    p1.login(w1)
    p2 = Player("Dwight")
    p2.login(w2)
    p3 = Player("Jim")
    p3.login(w1)
    p3.session_token = p1.session_token

    for p1, p2 in combinations((p1, p2, p3), 2):
        assert p1 != p2

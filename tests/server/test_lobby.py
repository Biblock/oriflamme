import pytest

from src.server.chat import MAX_MESSAGE_LENGTH
from src.server.events import player_joined_game_event, player_joined_lobby_event, player_session_token_event, \
    game_created_event, player_left_game_event, disconnected_event, player_already_in_a_game_event, \
    game_already_exists_event, player_not_in_game_event, max_player_limit_event, \
    TerminalEventError, \
    game_deleted_event, not_enough_players_event, player_is_not_game_leader_event, game_already_started_event, \
    NoSuchGame, chat_message_too_long_event
from src.server.game import Game
from src.server.server import Server


@pytest.fixture
def lobby():
    server = Server(None, None)
    return server.lobby


@pytest.fixture
def lobby_with_players(websockets, lobby):
    for i, ws in enumerate(websockets(15)):
        lobby.login(ws, f"player_{str(i)}")
    lobby.players[-1].logoff()
    return lobby


def reset_sockets(sockets):
    for ws in sockets:
        ws.reset_mock()


def test_login(websockets, lobby):
    w1, w2, w3, w4, w5 = sockets = websockets(5)

    # Dwight login
    assert not lobby.players
    lobby.login(w1, "Dwight")
    dwight = lobby.players[-1]

    assert dwight.name == "Dwight"
    assert dwight.logged
    w1.send.assert_any_call(player_joined_lobby_event(dwight), dwight)
    w1.send.assert_any_call(player_session_token_event(dwight, lobby), dwight)
    assert w1.send.call_count == 2
    reset_sockets(sockets)

    # Jim login
    lobby.login(w2, "Jim")
    jim = lobby.players[-1]

    assert jim.logged
    dwight.ws_wrapper.send.assert_called_once_with(player_joined_lobby_event(jim), dwight)
    jim.ws_wrapper.send.assert_any_call(player_joined_lobby_event(jim), jim)
    jim.ws_wrapper.send.assert_any_call(player_session_token_event(jim, lobby), jim)
    assert jim.ws_wrapper.send.call_count == 2
    reset_sockets(sockets)

    # Same websocket as Dwight
    lobby.login(w1, "Wigged Dwight")
    wigged_dwight = lobby.players[-1]

    assert wigged_dwight.logged
    assert dwight.logged
    w2.send.assert_called_once_with(player_joined_lobby_event(wigged_dwight), jim)
    w1.send.assert_any_call(player_joined_lobby_event(wigged_dwight), dwight)
    w1.send.assert_any_call(player_joined_lobby_event(wigged_dwight), wigged_dwight)
    w1.send.assert_any_call(player_session_token_event(wigged_dwight, lobby), wigged_dwight)
    assert w1.send.call_count == 3
    assert wigged_dwight.name == "Wigged Dwight"
    reset_sockets(sockets)

    # Dwight logs again with another name and websocket without disconnecting first
    lobby.login(w3, "Dwight2", dwight.session_token)

    assert dwight.logged
    old_dwight_disconnect = disconnected_event(dwight)
    # I have to do that because the renaming happens during the disconnection process. In the test, the renaming
    # has occurred so the name of the player that has been disconnected is forgotten
    old_dwight_disconnect["playerId"] = dwight.id
    w1.send.assert_any_call(player_joined_lobby_event(dwight), wigged_dwight)
    w1.send.assert_any_call(old_dwight_disconnect, wigged_dwight)
    w1.send.assert_any_call(old_dwight_disconnect, dwight)
    assert w1.send.call_count == 3
    assert w2.send.call_count == 2
    assert w3.send.call_count == 2
    reset_sockets(sockets)

    # Dwight logs again with another websocket but disconnect first
    lobby.logoff(dwight, send_disconnected_player=True)
    w1.send.assert_any_call(disconnected_event(dwight), wigged_dwight)
    w3.send.assert_any_call(disconnected_event(dwight), dwight)
    w2.send.assert_any_call(disconnected_event(dwight), jim)

    lobby.login(w4, "Dwight", dwight.session_token)

    w1.send.assert_any_call(player_joined_lobby_event(dwight), wigged_dwight)
    w2.send.assert_any_call(player_joined_lobby_event(dwight), jim)
    assert w3.send.call_count == 1  # w3 should have been forgotten, only the disconnect
    w4.send.assert_any_call(player_joined_lobby_event(dwight), dwight)
    w4.send.assert_any_call(player_session_token_event(dwight, lobby), dwight)
    assert w4.send.call_count == 2
    reset_sockets(sockets)

    # Someone logs in with a fake or unknown token
    lobby.login(w5, "Creed", "Completely normal token")
    creed = lobby.players[-1]

    assert creed.session_token is not None
    assert creed.session_token != "Completely normal token"
    assert creed.logged
    w5.send.assert_any_call(player_session_token_event(creed, lobby), creed)
    reset_sockets(sockets)


def test_as_dict(lobby_with_players):
    lobby = lobby_with_players
    lobby.create_game(lobby.players[0], "The Office")
    assert lobby.as_dict() == {
        "maxChatMessageLength": MAX_MESSAGE_LENGTH,
        "players": [p.as_dict() for p in lobby.players],
        "games": [game.as_dict() for game in lobby.games]}


def test_create_game(lobby_with_players):
    lobby = lobby_with_players
    p1, p2, *_ = lobby.players
    lobby.create_game(p1, "The Office")
    the_office = lobby.games[-1]
    assert the_office.name == "The Office"
    assert the_office.leader == p1
    assert p1 in the_office.players

    for player in lobby.logged_player:
        player.ws_wrapper.send.assert_any_call(game_created_event(the_office), player)
        player.ws_wrapper.send.assert_any_call(player_joined_game_event(p1, the_office), player)

        with pytest.raises(TerminalEventError) as err:
            lobby.create_game(p1, "Irrelevant")
        assert err.value.event == player_already_in_a_game_event(p1, the_office)

        with pytest.raises(TerminalEventError) as err:
            lobby.create_game(p2, "The Office")
        assert err.value.event == game_already_exists_event("The Office")


def test_join_game(lobby_with_players):
    lobby = lobby_with_players
    p1, p2, p3, *_ = lobby.logged_player
    sockets = [p.ws_wrapper for p in lobby.logged_player]
    px = lobby_with_players.logged_player[-1]
    lobby.create_game(p1, "The Office")
    lobby.create_game(px, "Parks and recreation")
    the_office, parks = lobby.games

    # Working
    lobby.join_game(p2, the_office.name)
    assert p2 in the_office.players
    for player in p1, p2:
        player.ws_wrapper.send.assert_any_call(player_joined_game_event(p2, the_office), player)
    reset_sockets(sockets)

    # Already in a game, (re)joining this game
    with pytest.raises(TerminalEventError) as err:
        lobby.join_game(p2, the_office.name)
    assert p2 in the_office.players
    assert err.value.event == player_already_in_a_game_event(p2, the_office)
    reset_sockets(sockets)

    # Already in a game, joining another game
    with pytest.raises(TerminalEventError) as err:
        lobby.join_game(p2, "Parks and recreation")
    assert p2 in the_office.players
    assert err.value.event == player_already_in_a_game_event(p2, the_office)
    reset_sockets(sockets)

    # Game doesn't exist
    with pytest.raises(NoSuchGame) as err:
        lobby.join_game(p3, "Michael Scott Paper Company")
    assert err.value.game_name == "Michael Scott Paper Company"
    reset_sockets(sockets)

    # 5 players max
    with pytest.raises(TerminalEventError) as err:
        for player in lobby.logged_player[2:Game.MAXIMUM_PLAYER_NUMBER + 1]:
            lobby.join_game(player, the_office.name)
    for p in lobby.logged_player[:Game.MAXIMUM_PLAYER_NUMBER]:
        assert p in the_office.players
    assert len(the_office.players) == Game.MAXIMUM_PLAYER_NUMBER
    assert err.value.event == max_player_limit_event(Game.MAXIMUM_PLAYER_NUMBER)
    reset_sockets(sockets)


def test_leave_game(lobby_with_players):
    lobby = lobby_with_players
    p1, p2, *_ = lobby.logged_player
    sockets = [p.ws_wrapper for p in lobby.logged_player]
    lobby.create_game(p1, "The Office")
    the_office = lobby.games[-1]
    lobby.join_game(p2, the_office.name)

    # Working
    lobby.leave_game(p2, the_office.name)
    assert p2 not in the_office.players
    for player in p1, p2:
        player.ws_wrapper.send.assert_any_call(player_left_game_event(p2, the_office), player)
    reset_sockets(sockets)

    # Game doesn't exist
    with pytest.raises(NoSuchGame) as err:
        lobby.leave_game(p2, "Michael Scott Paper Company")
    assert err.value.game_name == "Michael Scott Paper Company"
    reset_sockets(sockets)

    # Not in game
    with pytest.raises(TerminalEventError) as err:
        lobby.leave_game(p2, the_office.name)
    assert err.value.event == player_not_in_game_event(the_office, p2)
    reset_sockets(sockets)

    # Leader leaves, leader become p2
    lobby.join_game(p2, the_office.name)
    lobby.leave_game(p1, the_office.name)
    assert the_office.leader == p2
    reset_sockets(sockets)

    # Everybody left, game is destroyed
    lobby.leave_game(p2, the_office.name)
    assert the_office not in lobby.games
    for player in lobby.logged_player:
        player.ws_wrapper.send.assert_any_call(game_deleted_event(the_office.name), player)
    reset_sockets(sockets)


def test_start_game(lobby_with_players):
    lobby = lobby_with_players
    p1, p2, p3, *_ = lobby.logged_player
    sockets = [p.ws_wrapper for p in lobby.logged_player]
    lobby.create_game(p1, "The Office")
    the_office = lobby.games[-1]

    # Game does not exist
    with pytest.raises(NoSuchGame) as err:
        lobby.start_game(p1, "Michael Scott Paper Company")
    assert err.value.game_name == "Michael Scott Paper Company"

    # Not enough players
    lobby.join_game(p2, the_office.name)
    with pytest.raises(TerminalEventError) as err:
        lobby.start_game(p1, the_office.name)
    assert err.value.event == not_enough_players_event(Game.MINIMUM_PLAYER_NUMBER)

    # Starter is not the leader
    with pytest.raises(TerminalEventError) as err:
        lobby.start_game(p2, the_office.name)
    assert err.value.event == player_is_not_game_leader_event(p2)

    # Working
    lobby.join_game(p3, the_office.name)
    lobby.start_game(p1, the_office.name)
    assert the_office.started
    for p in the_office.players:
        engine_player = the_office.engine_player(p)
        assert engine_player is not None
    reset_sockets(sockets)

    # Game already started
    with pytest.raises(TerminalEventError) as err:
        lobby.start_game(p1, the_office.name)
    assert err.value.event == game_already_started_event(the_office)


def test_leaving_started_game(lobby_with_players):
    # Leaving a game when it's started should end it. Because the game is not meant to be played
    # A disconnection doesn't mean the player has left the game
    lobby = lobby_with_players
    p1, *_ = lobby.logged_player
    sockets = [p.ws_wrapper for p in lobby.logged_player]
    lobby.create_game(p1, "The Office")
    the_office = lobby.games[-1]
    for player in lobby.logged_player[1:3]:
        lobby.join_game(player, the_office.name)
    lobby.start_game(p1, the_office.name)
    lobby.leave_game(p1, the_office.name)

    assert the_office not in lobby.games
    for player in lobby.logged_player:
        player.ws_wrapper.send.assert_any_call(game_deleted_event(the_office.name), player)
    reset_sockets(sockets)


def test_lobby_chat(lobby_with_players):
    lobby = lobby_with_players
    p1, *_ = lobby.logged_player

    msg = "a" * MAX_MESSAGE_LENGTH
    lobby.send_lobby_chat_message(p1, msg)

    for player in lobby.logged_player:
        assert player.ws_wrapper.send.call_args_list[-1].args[0]["message"] == msg
        assert player.ws_wrapper.send.call_args_list[-1].args[0]["senderId"] == p1.id
        assert player.ws_wrapper.send.call_args_list[-1].args[0]["event"] == "LOBBY_CHAT_MESSAGE"

    with pytest.raises(TerminalEventError) as err:
        lobby.send_lobby_chat_message(p1, msg + "z")
    assert err.value.event == chat_message_too_long_event()


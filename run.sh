port=8888
docker run -d --rm --name oriflamme -p "$port":80 -v /etc/oriflamme/logs:/usr/src/app/logs -e ORIFLAMME_PORT=80 lecourtoisn/oriflamme:latest

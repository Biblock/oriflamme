# Oriflamme
Disclaimer: Some game relative events precise the game name when other don't.
The reason is that I initially designed some of them thinking it would be possible to join
several games and the other thinking it wouldn't. As of today it's not possible so you should
be able to figure out the game knowing that the player the event is intended to can
only join a single one.

Eventually, I will make possible to join several games at once and systematically 
specify the game the event is about.

### Bad queries
    {
      "action": "LOGIN",
      "event": "PARAMETER_MISSING",
      "parameter": "name"
    }
<!-- -->
    {
      "event": "INVALID_JSON_SYNTAX",
      "data": data,
    }
<!-- -->
    {
      "event": "NO_SUCH_ACTION",
      "action": action_name,
    }

    
### Login
#### Actions
If no session token is given or the session token is wrong, the server will consider the logging player to be a new 
player.

Names are not unique in the Lobby 

    {
      "action": "LOGIN",
      "name": "Dwight",
      "sessionToken": "623a1f69-92ea-44f7-9648-32fe78ed31a6" # optional
    }

#### Events
Once the player is logged, it's session token is sent back and **must be given to the server with every action**. 

    {
      "event": "PLAYER_SESSION_TOKEN",
      "sessionToken": "4e7c7635-b293-4a7d-8e06-70b077db21f8",
      "lobby": {
        "players": [
          {
            "id": "d0f15be5-649d-4297-a7c9-acf40aa30e25",
            "name": "Dwight",
            "logged": true
          },
          {
            "id": "73391503-e022-418b-ae5c-85958d1afa13",
            "name": "Michael",
            "logged": true
          }
        ],
        "game": {
          "name": "Flonkerton",
          "leaderId": "d0f15be5-649d-4297-a7c9-acf40aa30e25",
          "players": [
            "d0f15be5-649d-4297-a7c9-acf40aa30e25"
          ],
          "minPlayers": 3,
          "maxPlayers": 5
        },
      },
      "recipientId": "73391503-e022-418b-ae5c-85958d1afa13"
    }
 
Along with the session_token the whole lobby status is sent so you can reconstitute it.

A single websocket on the client side can manage several player. The recipient field specify the player meant 
to receive the event.

    {
      "event": "NO_SUCH_LOGGED_PLAYER",
      "sessionToken": "623a1f69-92ea-44f7-9648-32fe78ed31a6",
    }
<!-- -->
    {
      "event": "PLAYER_JOINED_LOBBY",
      "player": {
        "id": "2b313fde-2f58-49b7-9afe-516dd2708256",
        "name": "Jim",
        "logged": true
      },
      "recipientId": "73391503-e022-418b-ae5c-85958d1afa13"
    }
<!-- -->
    {
      "event": "DISCONNECTED",
      "playerId": "724fe2cf-8e36-4c52-bd78-f437f39da1be",
      "recipientId": "73391503-e022-418b-ae5c-85958d1afa13"
    }


### Lobby
#### Actions

    {
      "action": "CREATE_GAME",
      "gameName": "Flonkerton"
    }

The creator automatically joins the game, he won't receive the `PLAYER_JOINED_GAME` event.

    {
      "action": "JOIN_GAME",
      "gameName": "Flonkerton"
    }
 
Only 5 players can join a game

    {
      "action": "LEAVE_GAME",
      "gameName": "Flonkerton"
    }

    
If the creator leaves the game, the second player becomes the game leader. If the last player leaves the game, 
the game is deleted.

    {
      "action": "START_GAME",
      "gameName": "Flonkerton"
    }

Minimum 3 players

#### Events
 
    {
      "event": "GAME_CREATED",
      "game": {
        "name": "Flonkerton",
        "leaderId": "d0f15be5-649d-4297-a7c9-acf40aa30e25",
        "players": [
          "d0f15be5-649d-4297-a7c9-acf40aa30e25"
        ],
        "minPlayers": 3,
        "maxPlayers": 5
      },
      "recipientId": "73391503-e022-418b-ae5c-85958d1afa13"
    }
<!-- -->
    {
      "event": "PLAYER_JOINED_GAME",
      "playerId": "9e0bdab7-621a-47e9-bb47-0da4b7cfa957",
      "gameName": "Flonkerton",
      "recipientId": "9e0bdab7-621a-47e9-bb47-0da4b7cfa957"
    }
<!-- -->
    {
      "event": "GAME_DELETED",
      "gameName": "Flonkerton",
      "recipientId": "73391503-e022-418b-ae5c-85958d1afa13"
    }
<!-- -->
    {
      "event": "MAX_PLAYER_LIMIT",
      "maxNbPlayer": 5,
      "recipientId": "73391503-e022-418b-ae5c-85958d1afa13"
    }
<!-- -->
    {
      "event": "NOT_ENOUGH_PLAYERS",
      "minNbPlayer": 2,
      "recipientId": "73391503-e022-418b-ae5c-85958d1afa13"
    }
<!-- -->
    {
      "event": "PLAYER_IS_NOT_GAME_LEADER",
      "playerId": "724fe2cf-8e36-4c52-bd78-f437f39da1be",
      "recipientId": "73391503-e022-418b-ae5c-85958d1afa13"
    }
<!-- -->
    {
      "event": "GAME_ALREADY_STARTED",
      "gameName": "Flonkerton",
      "recipientId": "73391503-e022-418b-ae5c-85958d1afa13"
    }
<!-- -->
    {
      "event": "GAME_AS_NOT_STARTED",
      "gameName": "Flonkerton",
      "recipientId": "73391503-e022-418b-ae5c-85958d1afa13"
    }
<!-- -->
    {
      "event": "GAME_ALREADY_EXISTS",
      "gameName": "Flonkerton",
      "recipientId": "73391503-e022-418b-ae5c-85958d1afa13"
    }
<!-- -->
    {
      "event": "NO_SUCH_GAME",
      "gameName": "Flonkerton",
      "recipientId": "73391503-e022-418b-ae5c-85958d1afa13"
    }
<!-- -->
    {
      "event": "PLAYER_ALREADY_IN_GAME",
      "playerId": "724fe2cf-8e36-4c52-bd78-f437f39da1be",
      "gameName": "Flonkerton",
      "recipientId": "73391503-e022-418b-ae5c-85958d1afa13"
    }



### Game
#### Actions
    {
      "action": "PLAY_CARD",
      "cardName": "assassinat",
      "gameName": "Flonkerton",
      "positionName": "left" # or right
    }
<!-- -->
    {
      "action": "PLAY_ON",
      "cardName": "changeforme",
      "gameName": "Flonkerton",
      "targetCardId": "77c5900d-cfcb-42e9-ae25-0ca0eaf5f8fe"
    }
<!-- -->
    {
      "action": "CHOOSE_RESOLUTION",
      "gameName": "Flonkerton",
      "resolutionChoice": "reveal", # or doNotReveal
      "sessionToken": "875f7b9e-2443-4433-9b9c-cc391caa10f9"
    }
<!-- -->
    {
      "action": "CHOOSE_PLAYER",
      "gameName": "Flonkerton",
      "targetPlayerId": "66263eca-5259-4d88-9244-6cbdf04cc3e8"
    }
<!-- -->
    {
      "action": "CHOOSE_CARD",
      "cardId": "77c5900d-cfcb-42e9-ae25-0ca0eaf5f8fe",
      "gameName": "Flonkerton"
    }
<!-- -->
    {
      "action": "CHOOSE_DESTINATION",
      "destinationCardId": "6577c9a9-c9de-473a-beeb-279be8a637f1",
      "gameName": "Flonkerton",
      "positionName": "right",
      "targetCardId": "f0e57725-ad75-47b8-b50f-c297679b6394"
    }
#### Events
    {
      "event": "NO_SUCH_PLAYER",
      "playerId": 724fe2cf-8e36-4c52-bd78-f437f39da1be,
      "recipientId": "73391503-e022-418b-ae5c-85958d1afa13"
    }
<!-- -->
    {
      "event": "PLAYER_NOT_IN_GAME",
      "gameName": "Flonkerton",
      "playerId": 724fe2cf-8e36-4c52-bd78-f437f39da1be,
      "recipientId": "73391503-e022-418b-ae5c-85958d1afa13"
    }
<!-- -->
    {
      "event": "PLAYER_IS_NOT_GAME_LEADER",
      "playerId": 724fe2cf-8e36-4c52-bd78-f437f39da1be,
      "recipientId": "73391503-e022-418b-ae5c-85958d1afa13"
    }
<!-- -->
    {
      "event": "GAME_AS_NOT_STARTED",
      "gameName": "Flonkerton",
      "recipientId": "73391503-e022-418b-ae5c-85958d1afa13"
    }
<!-- -->
    {
      "event": "NOT_A_CARD_NAME",
      "cardName": "DRAGON_BLANC_AUX_YEUX_BLEUS",
      "recipientId": "73391503-e022-418b-ae5c-85958d1afa13"
    }
<!-- -->
    {
      "event": "NO_SUCH_CARD",
      "cardId": "66263eca-5259-4d88-9244-6cbdf04cc3e8",
      "recipientId": "73391503-e022-418b-ae5c-85958d1afa13"
    }
<!-- -->
    {
      "event": "NOT_A_POSITION_NAME",
      "positionName": "NOT_YOUR_LEFT_MY_LEFT",
      "recipientId": "73391503-e022-418b-ae5c-85958d1afa13"
    }
<!-- -->
    {
      "event": "INVALID_RESOLUTION_CHOICE",
      "resolutionChoice": "REVEAL_ONLY_TO_MICHAEL",
      "recipientId": "73391503-e022-418b-ae5c-85958d1afa13"
    }
<!-- -->
    {
      "event": "GAME_ENGINE_ERROR",
      "gameEngineError": "game_engine_error",
      "recipientId": "73391503-e022-418b-ae5c-85958d1afa13"
    }


### Game progress

The `GAME_STATUS` event is sent when a game starts and when a player reconnects while
he's in a game so the client can set/reset the game environment. It's the only
moment the player's hand is sent. It's only the only moment the whole board is
sent. The rest of the time, the client has to manage its own board model by
reacting appropriately to the event `PLAYER_PLAYED_CARD`, `CARD_DISCARDED` and so on.

The `phaseName` can be `PLACEMENT` or `RESOLUTION`. If it's `RESOLUTION`
, you will get the `activeCardId` and the `prompt` will either be
`REVEAL`, `CARD`, `PLAYER` or `MOVEMENT`. If it's `PLACEMENT`, the `prompt` 
will always be `PLACEMENT`.

    {
      "event": "GAME_STATUS",
      "players": [
        {
          "id": "00fa7a68-85fa-4f5f-b547-43f1d3be7474",
          "name": "Dwight",
          "logged": true,
          "color": "GREEN",
          "influence": 1
        },
        {
          "id": "17fb8f5f-fa3e-4b35-9357-3efd2dbdd217",
          "name": "Jim",
          "logged": true,
          "color": "YELLOW",
          "influence": 1
        },
        {
          "id": "482762ab-f4da-42d3-9945-4b97948034c9",
          "name": "Michael",
          "logged": true,
          "color": "BLACK",
          "influence": 1
        }
      ],
      "hand": [
        {
          "cardId": "42a46d80-8702-42c2-be49-426127fd3eaa",
          "playerId": "00fa7a68-85fa-4f5f-b547-43f1d3be7474",
          "revealed": false,
          "kind": "ASSASSINAT",
          "influence": 0
        },
        {
          "cardId": "aa5c8ddf-b506-4b81-8304-b4d94ec507d2",
          "playerId": "00fa7a68-85fa-4f5f-b547-43f1d3be7474",
          "revealed": false,
          "kind": "CHANGEFORME",
          "influence": 0
        },
        {
          "cardId": "7ebe46ad-eb88-4445-a62d-be460287c088",
          "playerId": "00fa7a68-85fa-4f5f-b547-43f1d3be7474",
          "revealed": false,
          "kind": "ESPION",
          "influence": 0
        },
        {
          "cardId": "74bb7e58-a219-482f-89a3-df27a9f744cb",
          "playerId": "00fa7a68-85fa-4f5f-b547-43f1d3be7474",
          "revealed": false,
          "kind": "DECRET_ROYAL",
          "influence": 0
        },
        {
          "cardId": "d0120c73-5150-4c2c-8ffd-91b93f9c163e",
          "playerId": "00fa7a68-85fa-4f5f-b547-43f1d3be7474",
          "revealed": false,
          "kind": "ARCHER",
          "influence": 0
        },
        {
          "cardId": "4f924f43-2595-4388-a6b6-22e1e78ea869",
          "playerId": "00fa7a68-85fa-4f5f-b547-43f1d3be7474",
          "revealed": false,
          "kind": "EMBUSCADE",
          "influence": 0
        },
        {
          "cardId": "9d1c0114-cf7f-45d4-8c05-fa40a4622ce4",
          "playerId": "00fa7a68-85fa-4f5f-b547-43f1d3be7474",
          "revealed": false,
          "kind": "SOLDAT",
          "influence": 0
        },
        {
          "cardId": "8f324674-6800-48cd-9ab1-e6a7f5c1e802",
          "playerId": "00fa7a68-85fa-4f5f-b547-43f1d3be7474",
          "revealed": false,
          "kind": "COMPLOT",
          "influence": 0
        },
        {
          "cardId": "74ff3ab6-eed5-42fe-9ada-e10d24410ff3",
          "playerId": "00fa7a68-85fa-4f5f-b547-43f1d3be7474",
          "revealed": false,
          "kind": "SEIGNEUR",
          "influence": 0
        },
        {
          "cardId": "b3ac8a7a-ae11-4d4e-9a3f-974e15e8e4bc",
          "playerId": "00fa7a68-85fa-4f5f-b547-43f1d3be7474",
          "revealed": false,
          "kind": "HERITIER",
          "influence": 0
        }
      ],
      "board": [],
      "phase": {
        "phaseName": "PLACEMENT",
        "activeCardId": null,
        "prompt": "PLACEMENT"
      },
      "round": {
        "roundNumber": 1,
        "playersOrder": [
          "00fa7a68-85fa-4f5f-b547-43f1d3be7474",
          "17fb8f5f-fa3e-4b35-9357-3efd2dbdd217",
          "482762ab-f4da-42d3-9945-4b97948034c9"
        ]
      },
      "playerTurnId": "00fa7a68-85fa-4f5f-b547-43f1d3be7474",
      "recipientId": "00fa7a68-85fa-4f5f-b547-43f1d3be7474"
    }
    
The `board` here is empty. When it's not, it's a list of stack, and a 
stack is a list of cards. Here is a card object.

    {
      "cardId": "9d1c0114-cf7f-45d4-8c05-fa40a4622ce4",
      "playerId": "00fa7a68-85fa-4f5f-b547-43f1d3be7474",
      "revealed": false,
      "kind": "SOLDAT",
      "influence": 0
    }

The field `kind` is only sent if the card is yours or revealed. The `influence`
is only sent if the card is not revealed.

<!-- -->
    {
      "event": "ROUND_STARTING",
      "round": {
        "roundNumber": 1,
        "playersOrder": [
          "00fa7a68-85fa-4f5f-b547-43f1d3be7474",
          "17fb8f5f-fa3e-4b35-9357-3efd2dbdd217",
          "482762ab-f4da-42d3-9945-4b97948034c9"
        ]
      },
      "recipientId": "00fa7a68-85fa-4f5f-b547-43f1d3be7474"
    }
<!-- -->
    {
      "event": "PHASE_STARTING",
      "phase": "PLACEMENT",
      "recipientId": "00fa7a68-85fa-4f5f-b547-43f1d3be7474"
    }
<!-- -->
    {
      "event": "PHASE_STARTING",
      "phase": "RESOLUTION",
      "recipientId": "00fa7a68-85fa-4f5f-b547-43f1d3be7474"
    }
<!-- -->
    {
      "event": "PLACEMENT_PLAYER_TURN",
      "playerId": "00fa7a68-85fa-4f5f-b547-43f1d3be7474",
      "recipientId": "00fa7a68-85fa-4f5f-b547-43f1d3be7474"
    }
<!-- -->
    {
      "event": "RESOLUTION_PLAYER_TURN",
      "playerId": "00fa7a68-85fa-4f5f-b547-43f1d3be7474",
      "cardId": "9d1c0114-cf7f-45d4-8c05-fa40a4622ce4",     # the active card
      "recipientId": "00fa7a68-85fa-4f5f-b547-43f1d3be7474"
    }
<!-- -->
    {
      "event": "PLAYER_PLAYED_CARD",
      "playerId": "00fa7a68-85fa-4f5f-b547-43f1d3be7474",
      "card": {
        "cardId": "9d1c0114-cf7f-45d4-8c05-fa40a4622ce4",
        "playerId": "00fa7a68-85fa-4f5f-b547-43f1d3be7474",
        "revealed": false,
        "kind": "SOLDAT",
        "influence": 0
      },
      "positionName": "LEFT",
      "relativeCardId": null,
      "recipientId": "00fa7a68-85fa-4f5f-b547-43f1d3be7474"
    }
<!-- -->
    {
      "event": "PLAYER_CHOSE_RESOLUTION",
      "playerId": "00fa7a68-85fa-4f5f-b547-43f1d3be7474",
      "resolution": "DO_NOT_REVEAL",      # or REVEAL
      "cardId": "9d1c0114-cf7f-45d4-8c05-fa40a4622ce4",
      "recipientId": "00fa7a68-85fa-4f5f-b547-43f1d3be7474"
    },
<!-- -->
    {
      "event": "CARD_INFLUENCE_UPDATE",
      "cardId": "9d1c0114-cf7f-45d4-8c05-fa40a4622ce4",
      "difference": 1,
      "influence": 1,
      "recipientId": "00fa7a68-85fa-4f5f-b547-43f1d3be7474"
    }
<!-- -->
    {
      "event": "REVEALED_CARD",
      "card": {
        "cardId": "981430c2-eaf5-4bad-b15a-6a57916e383c",
        "playerId": "17fb8f5f-fa3e-4b35-9357-3efd2dbdd217",
        "revealed": true,
        "kind": "HERITIER"
      },
      "recipientId": "00fa7a68-85fa-4f5f-b547-43f1d3be7474"
    }
<!-- -->
    {
      "event": "PROMPT_PLAYER",
      "playerId": "482762ab-f4da-42d3-9945-4b97948034c9",
      "cardId": "4644f893-0689-4d16-a045-35cfd61d6810",
      "recipientId": "00fa7a68-85fa-4f5f-b547-43f1d3be7474"
    }
<!-- -->
    {
      "event": "PROMPT_CARD",
      "playerId": "00fa7a68-85fa-4f5f-b547-43f1d3be7474",
      "cardId": "9d1c0114-cf7f-45d4-8c05-fa40a4622ce4",
      "recipientId": "00fa7a68-85fa-4f5f-b547-43f1d3be7474"
    }
<!-- -->
    {
      "event": "PROMPT_MOVEMENT",
      "playerId": "482762ab-f4da-42d3-9945-4b97948034c9",
      "cardId": "abe2d19e-8c40-4884-aae8-0e8b968d4bc5",
      "recipientId": "00fa7a68-85fa-4f5f-b547-43f1d3be7474"
    }

<!-- -->
    {
      "event": "REVEALED_CARD_ACTIVATION",
      "cardId": "abe2d19e-8c40-4884-aae8-0e8b968d4bc5",
      "recipientId": "00fa7a68-85fa-4f5f-b547-43f1d3be7474"
    }
<!-- -->
    {
      "event": "PLAYER_INFLUENCE_UPDATE",
      "playerId": "00fa7a68-85fa-4f5f-b547-43f1d3be7474",
      "difference": 1,
      "influence": 8,
      "recipientId": "00fa7a68-85fa-4f5f-b547-43f1d3be7474"
    }
<!-- -->
    {
      "event": "GAME_OVER",
      "scores": {
        "00fa7a68-85fa-4f5f-b547-43f1d3be7474": 8,
        "17fb8f5f-fa3e-4b35-9357-3efd2dbdd217": 7,
        "482762ab-f4da-42d3-9945-4b97948034c9": 4
      },
      "gameName": "Flonkerton",
      "recipientId": "00fa7a68-85fa-4f5f-b547-43f1d3be7474"
    }
    
    

### Chat
#### Actions
Send a message to all logged players.

    {
      "action": "SEND_LOBBY_CHAT_MESSAGE",
      "message": "Yo, dude"
    }
    
Send a message to the game players. The send must have joined the game.

    {
      "action": "SEND_GAME_CHAT_MESSAGE",
      "gameName": "Flonkerton"
      "message": "Yo, dude"
    }
#### Events
The send receive its own message too

    {
      "event": "LOBBY_CHAT_MESSAGE",
      "senderId": "00fa7a68-85fa-4f5f-b547-43f1d3be7474",
      "message": "Yo, dude",
      "recipientId": "00fa7a68-85fa-4f5f-b547-43f1d3be7474"
    }
<!-- -->
    {
      "event": "GAME_CHAT_MESSAGE",
      "senderId": "00fa7a68-85fa-4f5f-b547-43f1d3be7474",
      "message": "Yo, dude",
      "gameName": "Flonkerton",
      "recipientId": "482762ab-f4da-42d3-9945-4b97948034c9"
    }
